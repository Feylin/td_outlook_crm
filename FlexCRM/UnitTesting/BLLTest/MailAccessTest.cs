﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using FlexCRM.DAL;
using FlexCRM.DAL.EF;
using Microsoft.Office.Interop.Outlook;
using Moq;
using NUnit.Framework;

namespace UnitTesting.BLLTest
{
    public class MailAccessTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            var constr = ConfigurationManager.ConnectionStrings["TD_KanbanEntities"];
            DBContextHandler.SetConnectionString(constr.ConnectionString);
        }

        private TransactionScope singleTestScope;

        [SetUp]
        public void CreateTransactionScope()
        {
            singleTestScope = new TransactionScope();
        }

        [TearDown]
        public void DisposeTransactionScope()
        {
            singleTestScope.Dispose();
        }

        [Test]
        public void SaveEmailInDatabaseAndDirectory()
        {
            Mock<MailItem> mail = new Mock<MailItem>();

            Mock<Attachments> attach = new Mock<Attachments>();
            Mock<Attachment> attach1 = new Mock<Attachment>();
            attach.Setup(a => a.GetEnumerator()).Returns(new List<Attachment> {attach1.Object}.GetEnumerator());
            mail.Setup(m => m.Attachments).Returns(attach.Object);
            throw new NotImplementedException();
        }
    }
}
