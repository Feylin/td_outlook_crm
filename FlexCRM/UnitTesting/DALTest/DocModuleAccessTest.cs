﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlexCRM.DAL;
using NUnit.Framework;
using System.Configuration;
using System.Globalization;
using System.Transactions;

namespace UnitTesting.DALTest
{
    [TestFixture]
    public class DocModuleAccessTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            var constr = ConfigurationManager.ConnectionStrings["TD_KanbanEntities"];
            DBContextHandler.SetConnectionString(constr.ConnectionString);
        }

        private TransactionScope singleTestScope;

        [SetUp]
        public void CreateTransactionScope()
        {
            singleTestScope = new TransactionScope();
        }

        [TearDown]
        public void DisposeTransactionScope()
        {
            singleTestScope.Dispose();
        }

        [Test]
        public void SearchCompanyNametest()
        {
            string testString = "b";
            var list = DocModuleAccess.Instance.SearchCompanyName(testString);
            Assert.True(list.TrueForAll(dmc => 0 <= CultureInfo.CurrentCulture.CompareInfo.IndexOf(dmc.Name,testString,CompareOptions.IgnoreCase)));
            Assert.True(list.Any());

            testString = "bl";
            list = DocModuleAccess.Instance.SearchCompanyName(testString);
            Assert.True(list.TrueForAll(dmc => 0 <= CultureInfo.CurrentCulture.CompareInfo.IndexOf(dmc.Name, testString, CompareOptions.IgnoreCase)));
            Assert.True(list.Any());

        }


        [Test]
        public void FindDocModulesForCompanyTest()
        {
            string blue = "bluewater shipping a/s";

            var docModuleRefs = DocModuleAccess.Instance.SearchCompanyName(blue);

            var bluewater = docModuleRefs.FirstOrDefault();
            
            Assert.NotNull(bluewater);

            var modules = DocModuleAccess.Instance.DocStructureByCustomer(bluewater.ID);

            Assert.True(modules.Count == 6);

        }

    }
}
