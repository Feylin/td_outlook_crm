﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using FlexCRM.DAL;
using FlexCRM.DAL.EF;
using NUnit.Framework;
using CustomerContact = FlexCRM.DTO.CustomerContact;


namespace UnitTesting.DALTest
{
    [TestFixture]
    public class ContactAccessTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            var constr = ConfigurationManager.ConnectionStrings["TD_KanbanEntities"];
            DBContextHandler.SetConnectionString(constr.ConnectionString);
        }

        private TransactionScope singleTestScope;

        [SetUp]
        public void CreateTransactionScope()
        {
            singleTestScope = new TransactionScope();
        }

        [TearDown]
        public void DisposeTransactionScope()
        {
            singleTestScope.Dispose();
        }

        [Test]
        public void SearchByNameTest()
        {
            //tests that it works and the case is ignored
            var listContact = ContactAccess.Instance.SearchByName("MICHAEL LYSTER");
            Assert.True(listContact.Count == 1);
            Assert.True(listContact[0].Email == "mil@solar.dk");

        }

        [Test]
        public void SearchByEmailTest()
        {
            var contact = ContactAccess.Instance.GetContactByEmail("MIL@SOLAR.dk")[0];
            Assert.True(contact.Name == "michael Lyster");
            Assert.NotNull(contact as CustomerContact);

        }

        [Test]
        public void AddContactTest()
        {
            var conAccs = ContactAccess.Instance;
            conAccs.AddCustomerInformation("email@real.com","name lastname","18484848",new DocModulRef() {No = 1}, "mr.",1);

            var contact = conAccs.GetContactByEmail("email@real.com")[0] as CustomerContact;
            Assert.True(contact != null);
            Assert.That(contact.Name == "name lastname");
            Assert.True(contact.Phone == "18484848");
            Assert.True(contact.Company == 1);

           
        }
    }
}
