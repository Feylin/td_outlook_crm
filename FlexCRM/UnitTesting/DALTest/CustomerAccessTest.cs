﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using FlexCRM.DAL;
using NUnit.Framework;

namespace UnitTesting.DALTest
{
    [TestFixture]
    public class CustomerAccessTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            var constr = ConfigurationManager.ConnectionStrings["TD_KanbanEntities"];
            DBContextHandler.SetConnectionString(constr.ConnectionString);
        }

        private TransactionScope singleTestScope;

        [SetUp]
        public void CreateTransactionScope()
        {
            singleTestScope = new TransactionScope();
        }

        [TearDown]
        public void DisposeTransactionScope()
        {
            singleTestScope.Dispose();
        }


    }
}
