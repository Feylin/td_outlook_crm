﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using FlexCRM.BLL;
using FlexCRM.DAL;
using FlexCRM.DAL.EF;
using NUnit.Framework;

namespace UnitTesting.DALTest
{
    [TestFixture]
    public class DocumentAccessTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            var constr = ConfigurationManager.ConnectionStrings["TD_KanbanEntities"];
            DBContextHandler.SetConnectionString(constr.ConnectionString);
        }

        private TransactionScope singleTestScope;

        [SetUp]
        public void CreateTransactionScope()
        {
            singleTestScope = new TransactionScope();
        }

        [TearDown]
        public void DisposeTransactionScope()
        {
            singleTestScope.Dispose();
        }

        [Test]
        public void TestAddDocument()
        {
            var doc = new FlexCRM.BE.Document();
            DocModulRef company = DocModuleAccess.Instance.SearchCompanyName("bluewater")[0];
            doc.CompanyNo = company.No.Value;
            doc.Modul = company.Modul.Value;
            doc.ModulRef = company.ID;
            doc.DocStructureID = 2;//Quotation
            doc.Description = "Nunit test document";
            var followUpDate = DateTime.Now.AddDays(2);
            doc.FollowUpDate = followUpDate;
            doc.Anchor = "<a href=\"location\">link</a>";

            DocumentAccess.Instance.AddDoucment(doc);

            var fromDB = DBContextHandler.GetNewDBInstance()
                .Documents.Where(d => d.Description == "Nunit test document").FirstOrDefault();

            Assert.NotNull(fromDB);
            Assert.True(fromDB.Description == "Nunit test document");
            Assert.True(doc.FollowUpDate == followUpDate);
        }
    }
}
