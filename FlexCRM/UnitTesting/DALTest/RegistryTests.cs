using FlexCRM.DAL;
using NUnit.Framework;

namespace UnitTesting.DALTest
{
    [TestFixture]
    public class RegistryTests
    {
        private ISettingsHandler _settingsHandler;

        [TestFixtureSetUp]
        public void TestFixtureSetup()
        {
            _settingsHandler = SettingsHandler.Instance;
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }

        [TestCase("testStringOne", Result = true)]
        [TestCase("testStringTwo", Result = true)]
        [TestCase("testStringThree", Result = true)]
        public bool TestSaveToRegistry(string constring)
        {
            return _settingsHandler.SaveConfig(constring);
        }

        [TestCase(Result = "testStringOne")]
        [TestCase(Result = "testStringTwo")]
        [TestCase(Result = "testStringThree")]
        [TestCase(Result = null)]
        public string TestReadFromRegistry()
        {
            return _settingsHandler.ReadConfig();
        }

        [TestCase(Result = true)]
        [TestCase(Result = true)]
        [TestCase(Result = true)]
        [TestCase(Result = false)]
        public bool TestDeleteFromRegistry()
        {
            return _settingsHandler.DeleteConfig();
        }
    }
}
