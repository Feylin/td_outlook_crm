﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using UnitTesting.DALTest;

namespace UnitTesting
{
    /// <summary>
    /// A test class (suite) containing all of our tests
    /// </summary>
    class AllTests
    {
        [Suite]
        public static IEnumerable Suite
        {
            get
            {
                ArrayList suite = new ArrayList
                {
                    new ContactAccessTest(),
                    new CustomerAccessTest(),
                    new DocModuleAccessTest(),
                    new DocumentAccessTest(),
                    new RegistryTests()
                };
                return suite;
            }
        }
    }
}
