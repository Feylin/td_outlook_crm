﻿using System.Collections.Generic;
using FlexCRM.DAL;
using FlexCRM.DAL.EF;

namespace FlexCRM.BLL
{
    public class CompanySelectionViewModel
    {
        public List<DocModulRef> Search(string query)
        {
            return CustomerAccess.Instance.Search(query.ToLower());
        }
    }
}
