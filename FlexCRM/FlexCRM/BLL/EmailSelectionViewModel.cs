﻿using System.Collections.Generic;
using FlexCRM.DAL.EF;
using FlexCRM.DAL;

namespace FlexCRM.BLL
{
    public class EmailSelectionViewModel
    {
        public List<DocModulRef> Search(string query)
        {
            return CustomerAccess.Instance.Search(query.ToLower());
        }

        public List<DocModulRef> AutoSearch(string query)
        {
            return CustomerAccess.Instance.FindAllCompanyViaEmail(query.ToLower());
        }
    }
}
