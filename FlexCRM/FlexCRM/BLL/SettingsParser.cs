﻿using System.Data.SqlClient;
using FlexCRM.DAL;

namespace FlexCRM.BLL
{
    public class SettingsParser : ISettingsParser
    {
        private SettingsParser()
        {
        }

        public static readonly ISettingsParser Instance = new SettingsParser();

        public bool TestDatabaseConnection(string server, string db, string user, string pw, bool integratedSecurity)
        {

            var sqlConString = new SqlConnectionStringBuilder
            {
                InitialCatalog = db,
                DataSource = server,
                IntegratedSecurity = integratedSecurity,
                UserID = user,
                Password = pw,
            };

            return SettingsHandler.Instance.TestDatabaseConnection(sqlConString.ConnectionString);
        }

        public bool SaveDatabaseConnection(string server, string db, string user, string pw, bool integratedSecurity)
        {

            var sqlConString = new SqlConnectionStringBuilder
            {
                InitialCatalog = db,
                DataSource = server,
                IntegratedSecurity = integratedSecurity,
                UserID = user,
                Password = pw,
            };

            return SettingsHandler.Instance.SaveConfig(sqlConString.ConnectionString);
        }
    }
}
