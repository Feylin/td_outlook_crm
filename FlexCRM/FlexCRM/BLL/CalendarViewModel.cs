﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using FlexCRM.DAL;
using FlexCRM.DAL.EF;
using FlexCRM.GUI.StandAlone;
using FlexCRM.Util;
using Microsoft.Office.Interop.Outlook;

namespace FlexCRM.BLL
{
    public class CalendarViewModel
    {
        private static readonly Lazy<CalendarViewModel> Lazy = new Lazy<CalendarViewModel>(() => new CalendarViewModel());

        public static CalendarViewModel Instance = Lazy.Value;

        private MAPIFolderEvents_12_Event _calendarFolderEvent;
        private MAPIFolder _flexCalendarFolder;
        private readonly IActivityAccess _activityAccess = ActivityAccess.Instance;
        private readonly ILoginAccess _loginAccess = LoginAccess.Instance;
        private const string ActivityId = "aId";
        private const string User = "User";
        private const string Flex = "FLEX";
        private const string CustomerId = "cId";
        private const string Crm = "CRM";

        private CalendarViewModel()
        {
            AttachEvents();
        }

        /// <summary>
        /// Gets the primary calendar
        /// </summary>
        /// <returns>Returns the primary calendar as a MAPIFolder object</returns>
        private MAPIFolder GetPrimaryCalendar()
        {
            if (_flexCalendarFolder != null) return _flexCalendarFolder;
            _flexCalendarFolder = Globals.ThisAddIn.Application.Session.GetDefaultFolder(OlDefaultFolders.olFolderCalendar);
            AddUserPropertiesToCalendar(_flexCalendarFolder);
            return _flexCalendarFolder;
        }

        /// <summary>
        /// Adds user defined properties to the calendar to allow synchronization
        /// </summary>
        /// <param name="calendar">The calendar, MAPIFolder object, which the properties should be added to</param>
        private void AddUserPropertiesToCalendar(MAPIFolder calendar)
        {
            if (calendar.UserDefinedProperties.Find(ActivityId) == null)
                calendar.UserDefinedProperties.Add(ActivityId, OlUserPropertyType.olInteger);
            if (calendar.UserDefinedProperties.Find(CustomerId) == null)
                calendar.UserDefinedProperties.Add(CustomerId, OlUserPropertyType.olInteger);
            if (calendar.UserDefinedProperties.Find(User) == null)
                calendar.UserDefinedProperties.Add(User, OlUserPropertyType.olInteger);
            if (calendar.UserDefinedProperties.Find(Flex) == null)
                calendar.UserDefinedProperties.Add(Flex, OlUserPropertyType.olText);
        }

        /// <summary>
        /// Attaches events to the primary calendar
        /// </summary>
        private void AttachEvents()
        {
            _calendarFolderEvent = GetPrimaryCalendar() as MAPIFolderEvents_12_Event;
            if (_calendarFolderEvent != null) _calendarFolderEvent.BeforeItemMove += Before_ItemMove;
        }

        // Handles item deletion. Items moved to the deleted items folder
        // If this event is enabled together with our calendar_remove function all the removed appointments will also be deleted from the database
        private void Before_ItemMove(object item, MAPIFolder moveto, ref bool cancel)
        {
            //var appointment = item as AppointmentItem;

            //var activityIdProp = appointment?.UserProperties.Find(ActivityId);
            //var flexProp = appointment?.UserProperties.Find(Flex);

            //if (activityIdProp != null && flexProp != null && flexProp.Value == Crm)
            //{
            //    var cusActivityId = (long)activityIdProp.Value;
            //    _activityAccess.DeleteActivity(cusActivityId);
            //}
        }

        private Account GetAccountForFolder(MAPIFolder folder)
        {
            // Enumerate over the accounts defined for the session
            // Match the DefaultStore.StoreID of the account with the Store.StoreID for the currect folder.
            // Return the account whose default delivery store matches the store of the given folder.
            return Globals.ThisAddIn.Application.Session.Accounts.Cast<Account>().FirstOrDefault(account => account.DeliveryStore.StoreID == folder.Store.StoreID);
        }

        /// <summary>
        /// Compares calendar appointment to the corresponding customer activity in the database
        /// and updates one of them depending on modification date
        /// </summary>
        /// <param name="appointment">Appointment in the calendar</param>
        /// <param name="customerActivity">Customer activity in the database</param>
        private void CompareAppointment(AppointmentItem appointment, CustomerActivity customerActivity)
        {
            if (customerActivity.LastModified == null || appointment.LastModificationTime > customerActivity.LastModified)
            {
                customerActivity.Text = appointment.Body;
                customerActivity.Headline = appointment.Subject;
                customerActivity.Closed = appointment.Categories == null ? (DateTime?) null : DateTime.Now;
                _activityAccess.UpdateActivity(customerActivity);
            }
            else if (customerActivity.LastModified > appointment.LastModificationTime)
            {
                appointment.Body = customerActivity.Text;
                appointment.Subject = customerActivity.Headline;
                appointment.UserProperties.Find(User).Value = customerActivity.Responsible;
                appointment.UserProperties.Find(CustomerId).Value = customerActivity.Customer;
                appointment.Categories = customerActivity.Closed == null ? null : SharedVariables.Instance.ClosedCategory;
                appointment.Save();
            }
        }

        public Account GetAccountForPrimaryCalendar()
        {
            return GetAccountForFolder(GetPrimaryCalendar());
        }

        /// <summary>
        /// Takes all the customer activities from the database and creates appointments in the calendar
        /// </summary>
        /// <param name="activities">Dictionary containing all the customer activities</param>
        /// <param name="login">Creates appointments for the specified login/user</param>
        private void ImportAllAppointments(Dictionary<long, CustomerActivity> activities, Login login)
        {
            MAPIFolder primaryCalendar = GetPrimaryCalendar();

            foreach (var customerActivity in activities.Values)
                CreateAppointment(primaryCalendar, customerActivity, login);

            //Globals.ThisAddIn.Application.ActiveExplorer().SelectFolder(primaryCalendar);
            //Globals.ThisAddIn.Application.ActiveExplorer().CurrentFolder.Display();
        }

        /// <summary>
        /// Creates and appointment and adds it to the calendar.
        /// </summary>
        /// <param name="primaryCalendar">The primary calendar folder, default or custom calendar</param>
        /// <param name="activity">The customer activity object which contains created date among other things</param>
        /// <param name="login">The login of the current user</param>
        /// <param name="reminder">Reminder before appointment start, if reminder is null the outlook reminder is set to null and no sound is played</param>
        private void CreateAppointment(MAPIFolder primaryCalendar, CustomerActivity activity, Login login, int? reminder = null)
        {
            var newAppointment = Globals.ThisAddIn.Application.CreateItem(OlItemType.olAppointmentItem) as AppointmentItem;

            if (newAppointment != null)
            {
                if (activity.Followup != null) newAppointment.Start = (DateTime) activity.Followup;
                newAppointment.Subject = activity.Headline;
                newAppointment.Body = activity.Text;
                // newAppointment.Recipients.Add(GetAccountForFolder(primaryCalendar).DisplayName);
                newAppointment.Categories = activity.Closed == null ? null : SharedVariables.Instance.ClosedCategory;
                switch (reminder)
                {
                    case null:
                        newAppointment.ReminderSet = false;
                        newAppointment.ReminderPlaySound = false;
                        break;
                    default:
                        newAppointment.ReminderMinutesBeforeStart = reminder.GetValueOrDefault();
                        break;
                }
                // Guessing only all day events are possible looking at the database
                // newAppointment.AllDayEvent = true;
                newAppointment.UserProperties.Add(ActivityId, OlUserPropertyType.olInteger).Value = activity.ID;
                newAppointment.UserProperties.Add(CustomerId, OlUserPropertyType.olInteger).Value = activity.Customer;
                newAppointment.UserProperties.Add(User, OlUserPropertyType.olInteger).Value = login.ID;
                newAppointment.UserProperties.Add(Flex, OlUserPropertyType.olText).Value = Crm;

                newAppointment.Move(primaryCalendar);
            }
        }

        public void CreateAppointment(CustomerActivity activity, Login login, int? reminder = null)
        {
            CreateAppointment(GetPrimaryCalendar(), activity, login, reminder);
        }

        public void SynchronizeCalendar()
        {
            SynchronizeCalendar(GetPrimaryCalendar(), _loginAccess.GetCurrentUser());
        }

        /// <summary>
        /// Imports customer activities to the calendar
        /// If the calendar is empty all appointments will be imported
        /// Otherwise only the missing appointments will be importedS
        /// </summary>
        public void ImportCalendar()
        {
            var currentLogin = _loginAccess.GetCurrentUser();
            var activities = _activityAccess.GetCustomerActivitiesForResponsible(currentLogin.ID);

            if (!GetPrimaryCalendar().Items.Restrict("[" + User + "] = " + currentLogin.ID).OfType<AppointmentItem>().Any())
                ImportAllAppointments(activities, currentLogin);
            else
            {
                foreach (var customerActivity in activities.Values)
                {
                    if (MissingActivityIdInCalendar(customerActivity, GetPrimaryCalendar().Items.Restrict("[" + User + "] = " + currentLogin.ID).OfType<AppointmentItem>().ToList()))
                        CreateAppointment(GetPrimaryCalendar(), customerActivity, currentLogin);
                }
            }
        }

        /// <summary>
        /// Synchronizes the calendar with the database, and performs some standard cleanup
        /// For instance, if the calendar contains IDs of appointments not in the database they are removed
        /// If the follow date in the calendar does not match the follow up date in the database, the appointment is moved to the correct date
        /// </summary>
        /// <param name="calFolder">The calendar folder</param>
        /// <param name="login">Login of the user</param>
        public void SynchronizeCalendar(MAPIFolder calFolder, Login login)
        {
            var activites = _activityAccess.GetCustomerActivitiesForResponsible(login.ID);

            // TODO If you save a single reference of the restricted calendar items you would have to execute synchronize twice
            // If the appointment list contains an id not in the database -> remove it/them
            // or if the start date of the appointment does not match the follow up date of the activity with the same id -> remove it/them
            // because there is no move method to a new date they have to be removed first
            calFolder.Items.Restrict("[" + User + "] = " + login.ID).OfType<AppointmentItem>()
                    .Where(appointment => !activites.ContainsKey((long) appointment.UserProperties.Find(ActivityId).Value) 
                    || appointment.Start != activites[(long) appointment.UserProperties.Find(ActivityId).Value].Followup.GetValueOrDefault())
                    .ToList()
                    .ForEach(appointment => appointment.Delete());

            foreach (var customerActivity in activites.Values)
            {
                // Creates a restricted item list where the 'user' property of the appointment item has the ID of the logged in user as value
                var items = calFolder.Items.Restrict("[" + User + "] = " + login.ID).OfType<AppointmentItem>().ToList();

                // Loops through the calendar items and check for missing Activity IDs, and creates the appointments if there are some missing
                if (MissingActivityIdInCalendar(customerActivity, items))
                    CreateAppointment(calFolder, customerActivity, login);
                else
                {
                    foreach (var appointment in items)
                    {
                        if (customerActivity.ID == (long?) appointment?.UserProperties.Find(ActivityId).Value)
                        {
                            // Compares the appointments if the activity id user property matches the activity id found in the database
                            CompareAppointment(appointment, customerActivity);
                            break;
                        }
                    }
                }
            }

            RemoveObsoleteFlexAppointments();
        }

        private static bool MissingActivityIdInCalendar(CustomerActivity customerActivity, List<AppointmentItem> items)
        {
            return items.All(appointment => (long?)appointment.UserProperties.Find(ActivityId).Value != customerActivity.ID);
        }

        // TODO Should this remove all appointments or just invalid?
        /// <summary>
        /// Cleans up the calendar by removing all the flex appointments before the current date
        /// but only if they have been closed as well
        /// </summary>
        public void RemoveObsoleteFlexAppointments()
        {
            Items flexItems = GetPrimaryCalendar().Items.Restrict("[" + Flex + "] = " + Crm);

            for (int i = flexItems.Count; i > 0; i--)
            {
                AppointmentItem appointment = flexItems[i] as AppointmentItem;

                if (appointment?.Start < DateTime.Today && appointment.Categories == SharedVariables.Instance.ClosedCategory)
                    appointment.Delete();
            }
        }

        public void RemoveAllFlexAppointments()
        {
            Items flexItems = GetPrimaryCalendar().Items.Restrict("[" + Flex + "] = " + Crm);

            for (int i = flexItems.Count; i > 0; i--)
            {
                AppointmentItem appointment = flexItems[i] as AppointmentItem;
                appointment?.Delete();
            }
        }

        public void RemoveAllAppointments()
        {
            Items calendarItems = GetPrimaryCalendar().Items;

            for (int i = calendarItems.Count; i > 0; i--)
            {
                AppointmentItem appointment = calendarItems[i] as AppointmentItem;
                appointment?.Delete();
            }
                
        }

        public void UpdateActivityDependencies()
        {
            _activityAccess.FetchActivityTypes();
            _loginAccess.FetchLogins();
        }

        public void CreateActivity()
        {
            var window = new Window
            {
                SizeToContent = SizeToContent.WidthAndHeight,
                Content = new CreateActivity(),
                ResizeMode = ResizeMode.NoResize
            };
            window.ShowDialog();
        }
    }
}
