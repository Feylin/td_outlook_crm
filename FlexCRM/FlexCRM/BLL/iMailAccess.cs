﻿using Microsoft.Office.Interop.Outlook;

namespace FlexCRM.BLL
{
    interface IMailAccess
    {
        /// <summary>
        /// Get sender´s email.
        /// </summary>
        /// <param name="mail"></param>
        /// <returns></returns>
        string GetSenderSMTPAddress(MailItem mail);
    }
}
