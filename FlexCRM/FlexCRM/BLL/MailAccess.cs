﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.CompilerServices;
using FlexCRM.DAL;
using FlexCRM.DAL.EF;
using FlexCRM.Util;
using Microsoft.Office.Interop.Outlook;

namespace FlexCRM.BLL
{
    public class MailAccess : IMailAccess
    {
        public static MailItem CurrentMailItem;

        /// <summary>
        /// Save all file and .msg from email.
        /// Save information in database and in directory.
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="modul"></param>
        /// <param name="moduleRef"></param>
        /// <param name="structure"></param>
        /// <param name="followUpDate"></param>
        /// <param name="description"></param>
        public void SaveEmailInDatabaseAndDirectory(MailItem mail, DocModule modul, DocModulRef moduleRef,
                                                    DocStructure structure,DateTime? followUpDate, string description,long? activityID)
        {
            ValidateParameters(mail,  modul, moduleRef, structure);
            string tailOfPath = null;
            string headPath = EmailInformationReader(modul.DocModule1, moduleRef.Name, structure.Name, mail,out tailOfPath);
            Directory.CreateDirectory(headPath +"\\"+ tailOfPath  );

            List<BE.Document> docs = new List<BE.Document>();
            long? currentUserId = LoginAccess.Instance.GetCurrentUser()?.ID;

            foreach(Attachment at in mail.Attachments)
            {
                string[] name = at.FileName.Split('.');
                string tailPathToThisFile = tailOfPath + "\\" + name[0] + "_" + CleanString(DateTime.Now.ToString()) + "." + name[1];
                string entirePath = headPath + "\\" + tailPathToThisFile;
                at.SaveAsFile(entirePath);
                docs.Add(
                    new BE.Document
                    {
                        CompanyNo = moduleRef.No.Value,
                        Modul = modul.ID,
                        ModulRef = moduleRef.ID,
                        DocStructureID = structure.ID,
                        Description = null,
                        FollowUpDate = followUpDate,
                        Anchor = tailPathToThisFile,
                        CurrentADUser = currentUserId
                    });
            }

            string tailToMSG = tailOfPath +"\\"+ GetSenderSMTPAddress(mail) + "_" + CleanString(DateTime.Now.ToString()) + ".msg";
            mail.SaveAs(headPath + "\\" +tailToMSG, OlSaveAsType.olMSG);

            docs.Add(
                 new BE.Document
                 {
                     CompanyNo = moduleRef.No.Value,
                     Modul = modul.ID,
                     ModulRef = moduleRef.ID,
                     DocStructureID = structure.ID,
                     Description = description,
                     FollowUpDate = followUpDate,
                     CurrentADUser = currentUserId,
                     Anchor = tailToMSG,
                     Activity = activityID
                 });
            DocumentAccess.Instance.AddDoucment(docs.ToArray());
        }
        /// <summary>
        /// Throw exception if one of them is null: mail, modul, modulRef and structure.
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="modul"></param>
        /// <param name="moduleRef"></param>
        /// <param name="structure"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ValidateParameters(MailItem mail, DocModule modul, DocModulRef moduleRef, DocStructure structure)
        {
            if(mail == null)
                throw new ArgumentNullException(nameof(mail));
            if(modul == null)
                throw new ArgumentNullException(nameof(modul));
            if(moduleRef == null)
                throw new ArgumentNullException(nameof(moduleRef));
            if(structure==null)
                throw new ArgumentNullException(nameof(structure));
        }

       
        
        /// <summary>
        /// returns string where email to be save using parameters
        /// </summary>
        /// <param name="modul"></param>
        /// <param name="company"></param>
        /// <param name="structure"></param>
        /// <param name="mail"></param>
        /// <returns></returns>
        private string EmailInformationReader (string modul, string company, string structure, MailItem mail,out string tailPath)
        {
            string headPath = CustomerAccess.Instance.DocPath().Value;
            if (string.IsNullOrWhiteSpace(headPath))
                throw new FlexCRMException("No Document path set in Database");

            tailPath = modul + "\\" + CleanString(company) + "\\" + CleanString(structure) + "\\" +
                        GetSenderSMTPAddress(mail) + "\\" + CleanString(DateTime.Now.ToString());
            return headPath;
        }

        /// <summary>
        /// Pull all files in the mail.
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="index"></param>
        /// <param name="path"></param>
        public void EmailInformationSaveAttacgments(MailItem mail, int index, string path)
        {
            string[] name = mail.Attachments[index].FileName.Split('.');
            mail.Attachments[index].SaveAsFile(path + "\\" + name[0] + "_" + CleanString(DateTime.Now.ToString()) + "." + name[1]);
        }

        /// <summary>
        /// Save a copy of email as .msg file.
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public string EmailInformationSaveMsg(MailItem mail, string path)
        {
            string entirePath = path + "\\" + GetSenderSMTPAddress(mail) + "_" + CleanString(DateTime.Now.ToString()) + ".msg";
            mail.SaveAs(entirePath, OlSaveAsType.olMSG);
            return entirePath;
        }

        /// <summary>
        /// Clean text if contains this character.
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        private string CleanString(string module)
        {
            string illegal = "\\/.,:";
            StringBuilder builder = new StringBuilder(module);
            foreach (char c in illegal)
            {
                builder.Replace(c.ToString(), "");
            }
            return builder.ToString();
        }

        /// <summary>
        /// Get sender's email.
        /// </summary>
        /// <param name="mail"></param>
        /// <returns></returns>
        public string GetSenderSMTPAddress(MailItem mail)
        {
            string PR_SMTP_ADDRESS = @"http://schemas.microsoft.com/mapi/proptag/0x39FE001E";
            if (mail == null)
            {
                throw new ArgumentNullException();
            }
            if (mail.SenderEmailType == "EX")
            {
                AddressEntry sender = mail.Sender;
                if (sender != null)
                {
                    //Now we have an AddressEntry representing the Sender
                    if (sender.AddressEntryUserType == OlAddressEntryUserType.olExchangeUserAddressEntry ||
                        sender.AddressEntryUserType == OlAddressEntryUserType.olExchangeRemoteUserAddressEntry)
                    {
                        //Use the ExchangeUser object PrimarySMTPAddress
                        ExchangeUser exchUser = sender.GetExchangeUser();
                        return exchUser?.PrimarySmtpAddress;
                    }
                    return sender.PropertyAccessor.GetProperty(PR_SMTP_ADDRESS) as string;
                }
                return null;
            }
            return mail.Sender.Address;
        }
    }
}
