﻿namespace FlexCRM.BLL
{
    public interface ISettingsParser
    {
        /// <summary>
        /// Method used to test a database connection with the following arguments.
        /// </summary>
        /// <param name="server">Server to connect to</param>
        /// <param name="db">Underlying database and instance on the server</param>
        /// <param name="user">User login</param>
        /// <param name="pw">User password</param>
        /// <param name="integratedSecurity">When true, the current Windows account credentials are used for authentication</param>
        /// <returns>True if the database was tested successfully, returns false otherwise</returns>
        bool TestDatabaseConnection(string server, string db, string user, string pw, bool integratedSecurity);

        /// <summary>
        /// Saves the database connection using the parameters
        /// </summary>
        /// <param name="server">Server to connect to</param>
        /// <param name="db">Database and instance</param>
        /// <param name="user">Username</param>
        /// <param name="pw">Password</param>
        /// <param name="integratedSecurity">When true, the current Windows account credentials are used for authentication</param>
        /// <returns>Returns true if the database settings could be successfully saved to the registry, returns false otherwise</returns>
        bool SaveDatabaseConnection(string server, string db, string user, string pw, bool integratedSecurity);
    }
}
