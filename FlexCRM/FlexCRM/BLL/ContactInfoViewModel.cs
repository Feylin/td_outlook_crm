﻿using System.Text.RegularExpressions;

namespace FlexCRM.BLL
{
    public class ContactInfoViewModel
    {
        private const string EmailRegular = @"^(\w|\d)(\w|\d|(\.(\w|\d)))+@[a-z]+(.[a-z]*)*(.[a-z]{2,3})$";
        private const string NameRegular = @"^([a-zæøå]|[A-ZÆØÅ-]|\s)+$";
        private const string PhoneRegular = @"^(\+){0,1}[0-9]{10}|[0-9]{8}$";

        public bool ValidateEmail(string email)
        {
            return Regex.IsMatch(email, EmailRegular);
        }

        public bool ValidateName(string name)
        {
            return Regex.IsMatch(name, NameRegular);
        }

        public bool ValidatePhone(string phone)
        {
            return Regex.IsMatch(phone, PhoneRegular);
        }
    }
}
