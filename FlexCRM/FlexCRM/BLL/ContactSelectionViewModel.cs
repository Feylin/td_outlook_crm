﻿using System.Collections.Generic;
using FlexCRM.DAL;
using FlexCRM.DAL.EF;

namespace FlexCRM.BLL
{
    public class ContactSelectionViewModel
    {
        public List<DocModulContacts> SearchByName(string query)
        {
            return ContactAccess.Instance.SearchByName(query);
        }
    }
}
