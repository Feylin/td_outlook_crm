﻿namespace FlexCRM.DAL.EF
{
    partial class CustomerActivity
    {
        protected bool Equals(CustomerActivity other)
        {
            return Date.Equals(other.Date) && string.Equals(Headline, other.Headline) 
                && string.Equals(Text, other.Text) && Closed.Equals(other.Closed);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((CustomerActivity) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Date.GetHashCode();
                hashCode = (hashCode*397) ^ (Headline != null ? Headline.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Text != null ? Text.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ Closed.GetHashCode();
                return hashCode;
            }
        }
    }
}
