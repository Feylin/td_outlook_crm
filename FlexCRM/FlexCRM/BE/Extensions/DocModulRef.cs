﻿namespace FlexCRM.DAL.EF
{
    partial class DocModulRef
    {
        public override string ToString()
        {
            return Name;
        }

        protected bool Equals(DocModulRef other)
        {
            return ID == other.ID;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType()) return false;
            return Equals((DocModulRef) obj);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }
    }
}
