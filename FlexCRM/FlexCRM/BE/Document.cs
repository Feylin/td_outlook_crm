﻿using System;

namespace FlexCRM.BE
{
    public class Document
    {
        public string DocStructure { get; set; }
        public long DocStructureID { get; set; }
        public long? Activity { get; set; }
        public const long DocOrigin = 1;
        public long? CurrentADUser { get; set; }

        public long ModulRef { get; set; }
        public long Modul { get; set; }
        public long CompanyNo { get; set; }
        public string Description { get; set; }
        public DateTime? FollowUpDate { get; set; }

        public string Anchor { get; set; }
        public string DocPath { get; set; }
        
    }
}
