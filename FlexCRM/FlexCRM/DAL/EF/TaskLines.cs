//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FlexCRM.DAL.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class TaskLines
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TaskLines()
        {
            this.TimeStartStop = new HashSet<TimeStartStop>();
        }
    
        public long ID { get; set; }
        public long Task { get; set; }
        public Nullable<long> KanBan { get; set; }
        public Nullable<long> KanBanStatus { get; set; }
        public string Notes { get; set; }
        public Nullable<long> TDResp { get; set; }
        public Nullable<long> CUResp { get; set; }
        public Nullable<System.DateTime> DatePlanned { get; set; }
        public Nullable<System.DateTime> DateInProgress { get; set; }
        public Nullable<System.DateTime> DateDone { get; set; }
        public Nullable<decimal> Estimat { get; set; }
    
        public virtual CustomerContact CustomerContact { get; set; }
        public virtual KanBan KanBan1 { get; set; }
        public virtual KanBanStatus KanBanStatus1 { get; set; }
        public virtual Login Login { get; set; }
        public virtual Task Task1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TimeStartStop> TimeStartStop { get; set; }
    }
}
