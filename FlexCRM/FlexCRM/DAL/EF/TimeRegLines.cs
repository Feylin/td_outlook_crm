//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FlexCRM.DAL.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class TimeRegLines
    {
        public long ID { get; set; }
        public Nullable<long> TimeReg { get; set; }
        public long Customer { get; set; }
        public Nullable<long> TaskMain { get; set; }
        public long Task { get; set; }
        public long TaskLine { get; set; }
        public string Text { get; set; }
        public Nullable<decimal> Km { get; set; }
        public Nullable<decimal> Mon { get; set; }
        public Nullable<decimal> Tue { get; set; }
        public Nullable<decimal> Wed { get; set; }
        public Nullable<decimal> Thu { get; set; }
        public Nullable<decimal> Fri { get; set; }
        public Nullable<decimal> Sat { get; set; }
        public Nullable<decimal> Sun { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<System.DateTime> DateTime { get; set; }
        public System.DateTime Created { get; set; }
    
        public virtual Customer Customer1 { get; set; }
        public virtual CustomerMainTask CustomerMainTask { get; set; }
        public virtual KanBan KanBan { get; set; }
        public virtual Task Task1 { get; set; }
        public virtual TimeReg TimeReg1 { get; set; }
    }
}
