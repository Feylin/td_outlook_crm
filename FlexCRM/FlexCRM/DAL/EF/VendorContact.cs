//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FlexCRM.DAL.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class VendorContact
    {
        public long ID { get; set; }
        public Nullable<long> Vendor { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Title { get; set; }
        public string Mail { get; set; }
        public string Responsible { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public Nullable<bool> Active { get; set; }
    
        public virtual Vendor Vendor1 { get; set; }
    }
}
