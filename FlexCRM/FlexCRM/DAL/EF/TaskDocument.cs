//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FlexCRM.DAL.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class TaskDocument
    {
        public long ID { get; set; }
        public Nullable<long> Task { get; set; }
        public Nullable<long> Taskline { get; set; }
        public string FileName { get; set; }
        public byte[] Image { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public string Description { get; set; }
    
        public virtual Task Task1 { get; set; }
    }
}
