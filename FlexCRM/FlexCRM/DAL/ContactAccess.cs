﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlexCRM.DAL.EF;
using FlexCRM.DTO;
using CustomerContact = FlexCRM.DAL.EF.CustomerContact;
using VendorContact = FlexCRM.DAL.EF.VendorContact;

namespace FlexCRM.DAL
{
    public class ContactAccess
    {
        private static ContactAccess _Instance;
        private static object lockObj = new object();

        private ContactAccess()
        {    
        }

        public static ContactAccess Instance
        {
            get
            {
                if (_Instance == null)
                {
                    lock (lockObj)
                    {
                        if (_Instance == null)
                        {
                            _Instance = new ContactAccess();
                        }
                    }
                }
                return _Instance;
            }
        }

        /// <summary>
        /// searches docModulContacts for an exact match on email
        /// and for modul values 1,2 return a customer and vendor respectivly
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public List<ModuleContact> GetContactByEmail(string email)
        {
            try
            {
                using (var context = DBContextHandler.GetNewDBInstance())
                {
                    var contacts =
                        (from dmc in context.DocModulContacts
                            where dmc.Email.Equals(email, StringComparison.InvariantCultureIgnoreCase)
                            select dmc).ToList();

                    var list = new List<ModuleContact>();

                    foreach (var contact in contacts)
                    {
                        switch (contact.Modul.Value)
                        {
                            case 1:
                                var customerContact = context.CustomerContact.Find(contact.Contact.Value);
                                list.Add(contactFrom(customerContact, contact));
                                break;

                            case 2:
                                list.Add(new DTO.VendorContact()
                                {
                                    Vendor_ID = contact.No.Value,
                                    Email = contact.Email,
                                    Name = contact.Name,
                                    Modul = contact.Modul.Value,
                                    Company = contact.No
                                });
                                break;
                        }
                    }
                    return list;
                }
            }
            catch (Exception exe)
            {
                Logging.NLogger.Instance.Logger.Error(exe);
                throw;
            }
            
        }

        /// <summary>
        /// searches in contacts by name returns a page of DocModuleContacts (10 if comment matches implementation).
        /// </summary>
        /// <param name="query">substring of the name</param>
        /// <returns></returns>
        public List<DocModulContacts> SearchByName(string query)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                return (from dmc in context.DocModulContacts
                    where dmc.Name.Contains(query)
                    select dmc).Take(10).ToList();
            }
        }

        public DTO.CustomerContact AddCustomerInformation(string email, string name, string phone,DocModulRef company,string title,long module)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            using(var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    CustomerContact cust = new CustomerContact();
                    DocModulContacts dmc = new DocModulContacts();
                    cust.Email = email;
                    cust.Name = name;
                    cust.Phone = phone;
                    cust.Customer = company.No;
                    cust.Titel = title;
                    context.CustomerContact.Add(cust);
                    //assume this is needed to get ID
                    context.SaveChanges();

                    dmc.Modul = module;//TODO ID of customer module is hardcoded... maybe don't
                    dmc.Email = email;
                    dmc.Name = name;
                    dmc.No = company.No;
                    dmc.Contact = cust.ID;
                
                    context.DocModulContacts.Add(dmc);
                    context.SaveChanges();
                    transaction.Commit();
                    return contactFrom(cust, dmc);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    Logging.NLogger.Instance.Logger.Error(e);
                    throw;
                }
            }
        }

        private DTO.CustomerContact contactFrom(EF.CustomerContact customerContact, DocModulContacts contact)
        {
            return new DTO.CustomerContact()
            {
                ID = contact.ID,
                Company = contact.No.Value,
                CustomerContact_ID = contact.Contact.Value,
                Email = contact.Email,
                Name = contact.Name,
                Phone = customerContact.Phone,
                Modul = contact.Modul.Value,
                Title = customerContact.Titel
            };
        }

        private DTO.VendorContact contactFrom(EF.VendorContact vendorContact, DocModulContacts contact)
        {
            return new DTO.VendorContact()
            {
                ID = contact.ID,
                Vendor_ID = contact.No.Value,
                VendorContact_ID = contact.Contact.Value,
                Email = contact.Email,
                Name = contact.Name,
                Phone = vendorContact.Phone,
                Modul = contact.Modul.Value,
                Title = vendorContact.Title,               
            };
        }

        public ModuleContact getById(long id)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                var contact = context.DocModulContacts.Find(id);
                if (contact == null)
                {
                    return null;
                }
                switch (contact.Modul.Value)
                {
                    case 1:
                        {
                            var customerContact = context.CustomerContact.Find(contact.Contact);
                            return contactFrom(customerContact, contact);
                        }
                    case 2:
                        {
                            return new DTO.VendorContact()
                            {
                                Vendor_ID = contact.No.Value,
                                Email = contact.Email,
                                Name = contact.Name,
                            };
                        }
                }
                return new ModuleContact()
                {
                    Contact = contact.Contact,
                    Name = contact.Name,
                    Email = contact.Email,
                    ID = contact.ID,
                    Modul = contact.Modul.Value,
                    Company = contact.No
                }; 
            }
        }

        public DTO.VendorContact AddVendorInformation(string email, string name, string phone, DocModulRef modref, string title, string mobile,long module)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            using (var transaction = context.Database.BeginTransaction())
            {

                try
                {
                    VendorContact vend = new VendorContact();
                    DocModulContacts dmc = new DocModulContacts(); ;
                    vend.Mail = email;
                    vend.Name = name;
                    vend.Phone = phone;
                    vend.Vendor = modref.No;
                    vend.Title = title;
                    vend.Mobile = mobile;
                    vend.Active = true;
                    vend.Created = DateTime.Now;
                    context.VendorContact.Add(vend);
                    //assume this is needed to get ID
                    context.SaveChanges();

                    dmc.Modul = module;
                    dmc.Email = email;
                    dmc.Name = name;
                    dmc.No = modref.No;
                    dmc.Contact = vend.ID;

                    context.DocModulContacts.Add(dmc);
                    context.SaveChanges();
                    transaction.Commit();
                    return contactFrom(vend, dmc);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    Logging.NLogger.Instance.Logger.Error(e);
                    throw;
                }
            }
        }

        public ModuleContact AddDocModuleRefInformation(string email, string name, long? CompanyID,  long module)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                var dmc = new DocModulContacts();
                dmc.Modul = module;
                dmc.Email = email;
                dmc.Name = name;
                dmc.No = CompanyID;
                dmc.Contact = null;
                context.DocModulContacts.Add(dmc);
                context.SaveChanges();
                return new ModuleContact()
                {
                    Contact = dmc.Contact,
                    Name = dmc.Name,
                    Email = dmc.Email,
                    ID = dmc.ID,
                    Modul = dmc.Modul.Value,
                    Company = dmc.No
                };
            }
        }
    }
}
