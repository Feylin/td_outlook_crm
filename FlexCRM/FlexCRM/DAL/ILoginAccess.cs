﻿using System.Collections.Generic;
using FlexCRM.DAL.EF;
using FlexCRM.DTO;

namespace FlexCRM.DAL
{
    public interface ILoginAccess
    {
        /// <summary>
        /// Returns a dictionary of logins
        /// </summary>
        /// <returns>Returns a dictionary of logins where the key is id and value is an user object</returns>
        Dictionary<long, User> GetLogins();

        /// <summary>
        /// Fecthes all the logins from the database and stores them in a dictionary
        /// </summary>
        void FetchLogins();

        /// <summary>
        /// Retrieves the first matching user from the database
        /// Tries to match the login name in the table with the current logged in machine user
        /// </summary>
        /// <returns>Returns a login object if a user could be found in the database, returns null otherwise</returns>
        Login GetCurrentUser();
    }
}
