﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlexCRM.BE;
using FlexCRM.DAL.EF;
using FlexCRM.DTO;
using FlexCRM.Logging;

namespace FlexCRM.DAL
{
    public class DocumentAccess
    {
        private DocumentAccess()
        {
            
        }
        private static DocumentAccess _Instance;
        private static object lockObj = new object();
        private static DocOrigin DefaultOrigin = null;    

        public static DocumentAccess Instance
        {
            get
            {
                if (_Instance == null)
                {
                    lock (lockObj)
                    {
                        //second check to prevent using locks in every call to instance.
                        if (_Instance == null)
                        {
                            _Instance = new DocumentAccess();
                            DefaultOrigin = _Instance.FindOrigin("Email");
                            if (DefaultOrigin == null)
                            {
                                NLogger.Instance.Logger
                                    .Warn("Failed to find a DocOrigin with DocOrigin.Origin = \"Email\" in the database");
                            }
                        }
                    }
                }
                return _Instance;
            }
        }

        private DocOrigin FindOrigin(string originName)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                return context.DocOrigin.FirstOrDefault(o => o.Origin.Equals(originName));
            }
        }

        public void AddDoucment(params Document[] documents)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                //1 is the value of the customer module
                long? CustomerNo = documents[0].Modul == 1 ? documents[0].CompanyNo : new long?();
                foreach (var document in documents)
                {
                    context.Documents.Add(new Documents
                    {
                        //Referenced tables
                        No = document.CompanyNo, 
                        Customer = CustomerNo,
                        DocOrigin = DefaultOrigin?.ID,
                        Module = document.Modul,
                        RefID = document.ModulRef,
                        DocStructure = document.DocStructureID,
                        CreatedBy = document.CurrentADUser,

                        //columns
                        FileName = document.Anchor,
                        Reference = "",
                        Description = document.Description,
                        DocPath = null,
                        Activity = document.Activity,
                        FollowUpDate = document.FollowUpDate,

                        //weird redundant time-stamp columns
                        Created = DateTime.Now,
                        Date = DateTime.Now
                    });
                }
                context.SaveChanges();
            }
        }

        public void SaveModuleRef(DocModulRef moduleRef)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                try
                {
                    context.DocModulRef.Add(moduleRef);
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    NLogger.Instance.Logger.Error(e);
                    throw;
                }
            }
        }

        public List<DocModulRef> SearchModuleRefs(string text)
        {
            using(var context = DBContextHandler.GetNewDBInstance())
            try
            {
                return context.DocModulRef.Where(dmr => dmr.Name.StartsWith(text)).OrderBy(dmr => dmr.Name).ToList();
            }
            catch (Exception exe)
            {
                NLogger.Instance.Logger.Error(exe);
                throw;
            }
        }

        public object FindCompanyByNoAndModule(long No, long modul)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            try
            {
                return context.DocModulRef.Where(r => r.No == No).FirstOrDefault(r => r.Modul == modul);
            }
            catch (Exception exe)
            {
                NLogger.Instance.Logger.Error(exe);
                throw;
            }
        }

        public List<DocModulRef> searchCompany(string text, long modul,int listSize = 5)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
                try
                {
                    return context.DocModulRef.Where(r => r.Modul == modul).OrderBy(r => r.Name).Where(r => r.Name.StartsWith(text)).Take(listSize).ToList();
                }
                catch (Exception exe)
                {
                    NLogger.Instance.Logger.Error(exe);
                    throw;
                }
        }

        /// <summary>
        /// literally retarded
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        public List<DocModulRef> FindModuleRefsByContacts(List<ModuleContact> contact)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
                try
                {
                    var filteredList = contact.Where(r => r.Company != null).ToList();

                    var resultList = new List<DocModulRef>();
                    foreach (var pair in filteredList)
                        resultList.Add(context.DocModulRef.FirstOrDefault(dmr => dmr.No == pair.Company && dmr.Modul == pair.Modul));
                    return resultList.Distinct().ToList();
                }
                catch (Exception exe)
                {
                    NLogger.Instance.Logger.Error(exe);
                    throw;
                }
        }
    }
}
