﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using FlexCRM.DAL.EF;
using FlexCRM.Logging;

namespace FlexCRM.DAL
{
    public class ActivityAccess : IActivityAccess
    {
        private static readonly Lazy<ActivityAccess> Lazy = new Lazy<ActivityAccess>(() => new ActivityAccess());

        public static IActivityAccess Instance => Lazy.Value;

        private readonly Dictionary<long, CustomerActivityType> _customerActivityTypes = new Dictionary<long, CustomerActivityType>();

        private ActivityAccess()
        {
        }

        public CustomerActivity GetActivityById(long id)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                return context.CustomerActivity.FirstOrDefault(act => act.ID == id);
            }
        }

        public long SaveActivity(CustomerActivity cusActivity)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        context.CustomerActivity.Add(cusActivity);
                        context.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        NLogger.Instance.Logger.Error(ex);
                        throw;
                    }
                }
            }

            return cusActivity.ID;
        }

        public void UpdateActivity(CustomerActivity cusActivity)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var databaseActivity = context.CustomerActivity.SingleOrDefault(i => i.ID == cusActivity.ID);

                        if (databaseActivity != null)
                        {
                            context.CustomerActivity.Attach(databaseActivity);
                            context.Entry(databaseActivity).CurrentValues.SetValues(cusActivity);
                            context.SaveChanges();
                            transaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        NLogger.Instance.Logger.Error(ex);
                        throw;
                    }
                }
            }
        }

        public void DeleteActivity(CustomerActivity cusActivity)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        context.CustomerActivity.Remove(cusActivity);
                        context.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        NLogger.Instance.Logger.Error(ex);
                        throw;
                    }
                }
            }
        }

        public List<CustomerActivity> GetActivities(Expression<Func<CustomerActivity, bool>> filter = null)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                IQueryable<CustomerActivity> query = context.CustomerActivity;

                if (filter != null)
                    query = query.Where(filter);

                return query.ToList();
            }
        }

        // TODO What are the chances of new activity types being added? If non-existant this can be a class variable
        public void FetchActivityTypes()
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                foreach (var customerActivityType in context.CustomerActivityType)
                    _customerActivityTypes[customerActivityType.ID] = customerActivityType;
            }
        }

        public Dictionary<long, CustomerActivityType> GetActivityTypes()
        {
            return _customerActivityTypes.OrderBy(i => i.Value.ActivityType).ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        public CustomerActivity ContainExistingActivity(CustomerActivity cusActivity)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                return
                    context.CustomerActivity.FirstOrDefault(
                        activity => context.CustomerActivity.Any(act => act.Equals(cusActivity)));
            }
        }

        public void DeleteActivity(long databaseID)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        CustomerActivity cus = new CustomerActivity { ID = databaseID };
                        context.Entry(cus).State = EntityState.Deleted;
                        context.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception exe)
                    {
                        transaction.Rollback();
                        NLogger.Instance.Logger.Error(exe);
                        throw;
                    }
                }
            }
        }

        public Dictionary<long, CustomerActivity> GetCustomerActivitiesForResponsible(long id)
        {
            return GetActivities(activity => activity.Followup >= DateTime.Today || activity.Closed == null)
                   .Where(ca => ca.Responsible == id)
                   .ToDictionary(ca => ca.ID);
        }
    }
}
