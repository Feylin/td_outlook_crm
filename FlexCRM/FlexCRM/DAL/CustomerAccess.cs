﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlexCRM.DAL.EF;
using FlexCRM.Logging;

namespace FlexCRM.DAL
{
    public class CustomerAccess
    {
        private static CustomerAccess _Instance;
        private static object lockForSingleton = new object();
        private List<DocModulContacts> EmailQuery;

        public static CustomerAccess Instance
        {
            get
            {
                if (_Instance != null)
                {
                    return _Instance;
                }
                lock (lockForSingleton)
                {
                    return _Instance ?? (_Instance = new CustomerAccess());
                }
            }
        }
        private CustomerAccess()
        {          
        }

        public List<DocModulRef> Search(string query)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                {
                    var s = from dmf in context.DocModulContacts.Distinct()
                    join cust in context.DocModulRef.Distinct()
                    on dmf.No equals cust.No
                            where dmf.Email.Contains(query) 
                            || dmf.Name.Contains(query)
                            select cust;
                    return s.ToList();
                }
            }
        }

        public List<DocModulRef> FindAllCompanyViaEmail(string email)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                var s = (from dmr in context.DocModulRef
                         join cont in context.DocModulContacts
                         on dmr.No equals cont.No
                         where cont.Email == email
                         select dmr).Distinct();
                return s.ToList();
            }
        }

        public SystemParameter DocPath()
        {
            using (var docPath = DBContextHandler.GetNewDBInstance())
            {
                var p = docPath.SystemParameter.Where(path => path.ID == 11);
                return p.FirstOrDefault();
            }
        }

        public List<DocModule> GetAllModul()
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                var r = context.DocModule.Select(reff => reff);
                return r.ToList();
            }
        }

        /// <summary>
        /// only searches Customers NOT Vendors !!! 1
        /// </summary>
        /// <param name="nameFragment"></param>
        /// <param name="listSize"></param>
        /// <returns></returns>
        public List<Tuple<string, long>> SearchCompany(string nameFragment,int listSize = 5)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                var list = context.Customer
                    .Where(c => c.Name.StartsWith(nameFragment))
                    .Select(c => new {Name= c.Name,ID = c.ID})
                    .OrderBy(c => c.Name)
                    .Take(listSize);
                return list.AsEnumerable().Select(c => Tuple.Create(c.Name, c.ID)).ToList();
            }
        }

        public List<Tuple<string, long>> FindAllStatuses()
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                var list = context.CustomerStatus.ToList();
                return list.Select(status => Tuple.Create(status.Status, status.ID)).ToList();
            }
        }

        public List<Tuple<string, long>> FindAllCustomerGroups()
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                var list = context.CustomerGroup.ToList();
                return list.Select(status => Tuple.Create(status.CustomerGroup1, status.ID)).ToList();
            }
        }

        public void AddNewCustomer(Customer cust)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                try
                {
                    cust.Created = DateTime.Now;
                    context.Customer.Add(cust);
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    NLogger.Instance.Logger.Error(e);
                    throw;
                }
            }
        }

        public List<Tuple<string,long>> FindAllCustomerSubGroups(long groupID)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                var list = context.CustomerGroupSub.Where(cg=> cg.CustomerGroup == groupID).ToList();
                return list.Select(sg => Tuple.Create(sg.SubGroup, sg.ID)).ToList();
            }
        }

        public DocModulRef FindDocModuleRefByDomain(string senderEmail)
        {
            int lastAt = senderEmail.LastIndexOf('@');
            string domain = senderEmail.Substring(lastAt + 1);

            using(var context = DBContextHandler.GetNewDBInstance())
                try
                {
                    return (from cd in context.CustomerDomains
                    join dmr in context.DocModulRef
                        on cd.DocModuleRef_ID equals dmr.ID 
                    where cd.domain == domain
                    select dmr).FirstOrDefault();
                }
                catch (Exception exe)
                {
                    NLogger.Instance.Logger.Error(exe);
                    throw;
                }
        }

        public void AddCustomerDomains(string domainName, long refId)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
                try
                {
                    var custDom = new CustomerDomains
                    {
                        domain = domainName,
                        DocModuleRef_ID = refId
                    };
                    context.CustomerDomains.Add(custDom);
                    context.SaveChanges();
                }
                catch (Exception exe)
                {
                    throw;
                }
        }
    }
}
