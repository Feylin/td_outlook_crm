﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using FlexCRM.DAL.EF;

namespace FlexCRM.DAL
{
    public interface IActivityAccess
    {
        /// <summary>
        /// Retrieve a single customer activity object with the given id
        /// </summary>
        /// <param name="id">Id of the customer activity</param>
        /// <returns>Return the first customer activity object found, return null if none is found</returns>
        CustomerActivity GetActivityById(long id);

        /// <summary>
        /// Saves the customer activity to the database
        /// </summary>
        /// <param name="cusActivity">The customer activity object which is to be inserted</param>
        /// <returns>Returns the id of the inserted customer activity</returns>
        long SaveActivity(CustomerActivity cusActivity);

        /// <summary>
        /// Updates the customer activity in the database
        /// </summary>
        /// <param name="cusActivity">The customer activity object which is to be updates</param>
        void UpdateActivity(CustomerActivity cusActivity);

        /// <summary>
        /// Deletes the customer activity in the database
        /// </summary>
        /// <param name="cusActivity">The customer activity object which is to be deleted</param>
        void DeleteActivity(CustomerActivity cusActivity);

        /// <summary>
        /// Retrieve a list of customer activities with optional filter
        /// Filter example (activity => activity.Date > DateTime.Now)
        /// </summary>
        /// <param name="filter">An expression which filters the database query</param>
        /// <returns>Returns a list containing the filtered CustomerActivity objects</returns>
        List<CustomerActivity> GetActivities(Expression<Func<CustomerActivity, bool>> filter);

        /// <summary>
        /// Retrieves all the activity types from the database and saves them in a collection for later use
        /// </summary>
        void FetchActivityTypes();

        /// <summary>
        /// Retrieves the cached lsit of customer activities
        /// </summary>
        /// <returns>Returns a list of CustomerActivityType objects</returns>
        Dictionary<long, CustomerActivityType> GetActivityTypes();

        /// <summary>
        /// Checks the CustomerActivity table for existing activity
        /// </summary>
        /// <param name="cusActivity">The activity to check for in the database</param>
        /// <returns>True if existing activity is found, otherwise false</returns>
        CustomerActivity ContainExistingActivity(CustomerActivity cusActivity);

        /// <summary>
        /// Delete the activity, with the unique id, from the database
        /// </summary>
        /// <param name="databaseID">The unique id of the activity</param>
        void DeleteActivity(long databaseID);

        /// <summary>
        /// Retrieves customer actitives belonging to the responsible user
        /// The retrieved activities have not yet been closed (CustomerActivity.Closed == null) or
        /// the activities have a calendar start date which is today or after (CustomerActivity.Followup >= DateTime.Today)
        /// </summary>
        /// <param name="id">ID of the user responsible for maintaining the activities</param>
        /// <returns>A dictionary with activities </returns>
        Dictionary<long, CustomerActivity> GetCustomerActivitiesForResponsible(long id);
    }
}
