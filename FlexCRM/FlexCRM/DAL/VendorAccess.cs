﻿using System;
using FlexCRM.DAL.EF;
using FlexCRM.Logging;

namespace FlexCRM.DAL
{
    /// <summary>
    /// i guess basicly the same as Customer Access only for the vendor table
    /// </summary>
    public class VendorAccess
    {
        private static VendorAccess _instance;
        private static readonly object LockForSingleton = new object();

        private VendorAccess()
        {
        }

        public static VendorAccess Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (LockForSingleton)
                    {
                        if (_instance == null)
                        {
                            _instance = new VendorAccess();
                        }
                    }
                }              
                return _instance;
            }
        }

        public void AddVendor(Vendor vend)
        {
            try
            {
                using (var context = DBContextHandler.GetNewDBInstance())
                {
                    context.Vendor.Add(vend);
                    context.SaveChanges();
                }
            }
            catch (Exception exe)
            {
                NLogger.Instance.Logger.Error(exe);
                throw;
            }
        }
    }
}
