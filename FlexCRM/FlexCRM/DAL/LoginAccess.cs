﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlexCRM.DAL.EF;
using FlexCRM.DTO;
using FlexCRM.Logging;

namespace FlexCRM.DAL
{
    public class LoginAccess : ILoginAccess
    {
        private static readonly Lazy<LoginAccess> Lazy = new Lazy<LoginAccess>(() => new LoginAccess());

        public static ILoginAccess Instance = Lazy.Value;

        private readonly Dictionary<long, Login> _loginsDictionary = new Dictionary<long, Login>();

        private LoginAccess()
        {
        }

        public Dictionary<long, User> GetLogins()
        {
            return _loginsDictionary.Where(l => l.Value.Email != null || l.Value.Name != null)
                .Select(l => new User { Email = l.Value.Email, Id = l.Value.ID, Name = l.Value.Name })
                .OrderBy(l => l.Name).ToDictionary(l => l.Id);
        }

        public Login GetCurrentUser()
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                return context.Login.FirstOrDefault(l => l.Login1 == Environment.UserName);
            }
        }

        public void FetchLogins()
        {
            try
            {
                using (var context = DBContextHandler.GetNewDBInstance())
                {
                    foreach (var login in context.Login)
                        _loginsDictionary[login.ID] = login;
                }
            }
            catch (Exception ex)
            {
                NLogger.Instance.Logger.Error(ex);
                throw;
            }
        }
    }
}
