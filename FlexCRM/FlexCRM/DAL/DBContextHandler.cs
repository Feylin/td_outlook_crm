﻿using System;
using System.Data.Entity.Core.EntityClient;
using FlexCRM.DAL.EF;
using FlexCRM.Util;

namespace FlexCRM.DAL
{
    public class DBContextHandler
    {
        private DBContextHandler()
        {
        }
        
        private static object lockObj = new object();
        private static string _connectionString;

        /// <summary>
        /// manually sets the connection string.
        /// overwritting the beheaviour GetNewDBInstance() for unit tests.
        /// </summary>
        /// <param name="conString"></param>
#if DEBUG
        public static void SetConnectionString(string conString)
        {
            _connectionString = conString;
        }
#endif
        /// <summary>
        /// used when the connectionString has been updated so GetNewInstance() builds a new ConnectionString
        /// and returns a Context making use of that
        /// </summary>
        public static void ZeroConnectionString()
        {
            _connectionString = "";
        }
        public static TD_KanbanEntities GetNewDBInstance()
        {
            if (string.IsNullOrWhiteSpace(_connectionString))
            {
                lock (lockObj)
                {
                    if (string.IsNullOrWhiteSpace(_connectionString))
                    {
                        var currentDatabaseInfo = SettingsHandler.Instance.GetConnectionInfo();
                        if (currentDatabaseInfo == null)
                            return null;

                        var sqlConString = new System.Data.SqlClient.SqlConnectionStringBuilder
                        {
                            InitialCatalog = currentDatabaseInfo[SharedVariables.Instance.Database],
                            DataSource = currentDatabaseInfo[SharedVariables.Instance.Server],
                            IntegratedSecurity = false,
                            UserID = currentDatabaseInfo[SharedVariables.Instance.User],
                            Password = currentDatabaseInfo[SharedVariables.Instance.Password],
                        };

                        if (currentDatabaseInfo[SharedVariables.Instance.WinSecurity].Equals("true",StringComparison.OrdinalIgnoreCase))
                        {
                            sqlConString.IntegratedSecurity = true;
                        }

                        _connectionString = new EntityConnectionStringBuilder
                        {
                            Metadata = "res://*",
                            Provider = "System.Data.SqlClient",
                            ProviderConnectionString = sqlConString.ConnectionString
                        }.ConnectionString;
                    }
                }
            }
            return new TD_KanbanEntities(_connectionString);
        }
    }
}
