﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FlexCRM.DAL.EF;
using FlexCRM.Logging;

namespace FlexCRM.DAL
{
    public class DocModuleAccess
    {
        private static DocModuleAccess _instance;
        private static object _lockForSingleton = new object();

        //in the other version these were observable collections
        //but no callbacks were ever registered. so no idea why
        // -Jesper
        private List<DocModule> _docModules;
        private List<DocStructure> _docStructures;

        public static DocModuleAccess Instance
        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }
                lock (_lockForSingleton)
                {
                    return _instance ?? (_instance = new DocModuleAccess());
                }
            }
        }

        private DocModuleAccess()
        {
            Initialize();
        }

        private void Initialize()
        {
            try
            {
                using (TD_KanbanEntities context = DBContextHandler.GetNewDBInstance())
                {
                    _docModules = context.DocModule.Select(m => m).ToList();
                    _docStructures = context.DocStructure.Select(ds => ds).ToList();
                }
            }
            catch (Exception ex)
            {
                NLogger.Instance.Logger.Error(ex);
                throw;
            }
        }

        public List<DocModulRef> SearchCompanyName(string query)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                return context.DocModulRef.Where(mr => mr.Name.Contains(query)).Take(20).ToList();
            }
        }

        /// <summary>
        /// selects the docStructure elements available to a company
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public List<DocStructure> DocStructureByCustomer(long customerID)
        {
            using (var context = DBContextHandler.GetNewDBInstance())
            {
                List<long> modulesID = context.DocModulRef.Where(dmr => dmr.No == customerID).Select(dmr => dmr.Modul.Value).ToList();

                if (modulesID.Count > 0)
                    return context.DocStructure.Where(DS => modulesID.Contains(DS.DocModule.Value)).ToList();
                return null;
            }
        }

        public List<DocModule> GetDocModules()
        {
            return _docModules;
        }

        public List<DocStructure> StructureForModule(long module)
        {
            return _docStructures.Where(ds => ds.DocModule == module).ToList();
        }
    } 
}