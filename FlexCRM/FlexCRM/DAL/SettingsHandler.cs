﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using FlexCRM.DTO;
using FlexCRM.Logging;
using FlexCRM.Util;
using Microsoft.Win32;
using Exception = System.Exception;

namespace FlexCRM.DAL
{
    public class SettingsHandler : ISettingsHandler
    {
        private static readonly Lazy<SettingsHandler> Lazy = new Lazy<SettingsHandler>(() => new SettingsHandler());

        public static ISettingsHandler Instance => Lazy.Value;

        private SettingsHandler()
        {
        }

        // TODO This seems to not work correctly, returns unknown if currently logged-in user has no mail accounts associated
        public User GetCurrentOutlookUser()
        {
            var address = Globals.ThisAddIn.Application.Session.CurrentUser.AddressEntry;

            if (address.Type == "EX")
            {
                var currentUser = Globals.ThisAddIn.Application.Session.CurrentUser.AddressEntry.GetExchangeUser();

                if (currentUser != null)
                {
                    return new User
                    {
                        Email = currentUser.PrimarySmtpAddress,
                        Name = currentUser.Name
                    };
                }
            }
            else
            {
                var currentUser = Globals.ThisAddIn.Application.Session.CurrentUser.AddressEntry;

                if (currentUser != null)
                {
                    return new User
                    {
                        Email = currentUser.Address,
                        Name = currentUser.Name
                    };
                }
            }
            return null;
        }

        public string GetCurrentWindowsUser()
        {
            return Environment.UserName;
        }

        /// <summary>
        /// Replaces the %NAME% in the subkey with the parameter
        /// </summary>
        /// <param name="username">%NAME% is replaced with the username</param>
        /// <returns></returns>
        private string ReplaceKeyWithUser(string username)
        {
            string rootKey = SharedVariables.Instance.RootKey;
            string subKey = SharedVariables.Instance.SubKey;
            string key = rootKey + subKey;
            return key.Replace(subKey, username);
        }

        public string ReadConfig()
        {
            try
            {
                using (var key = Registry.LocalMachine.OpenSubKey(SharedVariables.Instance.RootKey))
                {
                    return key?.GetValue(SharedVariables.Instance.KeyName)?.ToString();
                }
            }
            catch (Exception ex)
            {
                NLogger.Instance.Logger.Error(ex);
                throw;
            }
        }

        public bool SaveConfig(string constring)
        {
            try
            {
                using (var key = Registry.LocalMachine.CreateSubKey(SharedVariables.Instance.RootKey))
                {
                    key?.SetValue(SharedVariables.Instance.KeyName, constring);
                    return true;
                }
            }
            catch (Exception ex)
            {
                NLogger.Instance.Logger.Error(ex);
                return false;
            }   
        }

        public bool DeleteConfig()
        {
            string rootKey = SharedVariables.Instance.RootKey;

            try
            {
                using (var key = Registry.LocalMachine.OpenSubKey(rootKey, true))
                {
                    if (key?.OpenSubKey(SharedVariables.Instance.KeyName) != null)
                    {
                        key.DeleteSubKeyTree(SharedVariables.Instance.KeyName);
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                NLogger.Instance.Logger.Error(ex);
                return false;
            }
        }

        public bool DatabaseSetup()
        {
            return TestDatabaseConnection(ReadConfig());
        }

        public bool TestDatabaseConnection(string connString)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connString))
                {
                    sqlConnection.Open();
                    return true;
                }
            }
            catch (SqlException ex)
            {
                NLogger.Instance.Logger.Error(ex);
                throw;
            }
            catch(Exception ex) when (ex is ArgumentException || ex is InvalidOperationException)
            {
                return false;
            }
        }

        public Dictionary<string, string> GetConnectionInfo()
        {
            var connProps = new Dictionary<string, string>();

            try
            {
                string connectionString = ReadConfig();
                var sqlConnectionStringBuilder = new SqlConnectionStringBuilder(connectionString);

                string server = SharedVariables.Instance.Server;
                string database = SharedVariables.Instance.Database;
                string user = SharedVariables.Instance.User;
                string password = SharedVariables.Instance.Password;
                string winSecurity = SharedVariables.Instance.WinSecurity;
                connProps.Add(winSecurity, sqlConnectionStringBuilder.IntegratedSecurity.ToString());
                connProps.Add(server, sqlConnectionStringBuilder.DataSource);
                connProps.Add(database, sqlConnectionStringBuilder.InitialCatalog);
                connProps.Add(user, sqlConnectionStringBuilder.UserID);
                connProps.Add(password, sqlConnectionStringBuilder.Password);
            }
            catch (Exception ex)
            {
                NLogger.Instance.Logger.Error(ex);
                throw;
            }
            return connProps;
        }
    }
}
