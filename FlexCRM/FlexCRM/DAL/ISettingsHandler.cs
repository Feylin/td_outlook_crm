﻿using System.Collections.Generic;
using FlexCRM.DTO;

namespace FlexCRM.DAL
{
    public interface ISettingsHandler
    {
        /// <summary>
        /// Returns the current user with outlook accounts on the local machine.
        /// Depending on the user type, default or exchange
        /// If no outlook email accounts are attached to the user, this method will return null
        /// Username would be "Unknown" in that case.
        /// </summary>
        /// <returns>Returns a user object with information of the current outlook user, if none found returns null</returns>
        User GetCurrentOutlookUser();

        /// <summary>
        /// Returns the user name of the current windows user
        /// </summary>
        /// <returns>Username in the form of a string</returns>
        string GetCurrentWindowsUser();

        /// <summary>
        /// Reads the database configuration from the registry
        /// </summary>
        /// <returns>Returns the registry value for the current user</returns>
        string ReadConfig();

        /// <summary>
        /// Overload method, which saves with the current user
        /// </summary>
        /// <param name="constring">Database connection string</param>
        /// <returns>Returns true or false depending on whether or not the database configuration could be saved in the registry</returns>
        bool SaveConfig(string constring);

        /// <summary>
        /// Deletes the database configuration in the registry database,
        /// For the user with the Username as subkey
        /// </summary>
        /// <returns>Returns true if the subkey in the registry could be deleted, false if it couldn't be deleted or if it doesn't exist</returns>
        bool DeleteConfig();

        /// <summary>
        /// Overload method, which tests the database setup with the current user
        /// </summary>
        /// <returns>Returns true if the database connection was tested successfully, false if it failed</returns>
        bool DatabaseSetup();

        /// <summary>
        /// Tests the database connecton with the supplied connection string,
        /// And returns true or false depending on the result.
        /// </summary>
        /// <param name="connString">The SQL connection string</param>
        /// <returns>/// <returns>Returns true if the database connection was tested successfully, false if it failed</returns></returns>
        bool TestDatabaseConnection(string connString);

        /// <summary>
        /// Method used to get the connection info from the current user,
        /// And return a dictionary with the information
        /// Server, database, username, password
        /// </summary>
        /// <returns>Returns a dictionary containing predefined keys, and values from the sql connection string read from the registry</returns>
        Dictionary<string, string> GetConnectionInfo();
    }
}
