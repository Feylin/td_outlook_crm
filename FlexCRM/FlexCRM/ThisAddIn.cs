﻿using System.Windows;
using FlexCRM.DAL;
using FlexCRM.DAL.EF;
using FlexCRM.GUI.StandAlone;
using FlexCRM.Util;
using Microsoft.Office.Interop.Outlook;
using MessageBox = System.Windows.MessageBox;

namespace FlexCRM
{
    public partial class ThisAddIn
    {
        private Inspectors _inspectors;
        public static MailItem CurrentMailItem;
        private readonly ISettingsHandler _settingsHandler = SettingsHandler.Instance;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            SetupCategories();

            _inspectors = Application.Inspectors;
            _inspectors.NewInspector += SetMailItem;

            // Slow start-up time if checking/testing for valid database connection
            if (string.IsNullOrEmpty(_settingsHandler.ReadConfig()))
            {
                const string message = "The database connection has not yet been been set.\nDo you want to setup the Connection?";
                const string title = "Thorvald Data FlexCRM - Database Setup";
                var mb = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Exclamation);

                if (mb == MessageBoxResult.Yes)
                    DatabaseConnection.CreateWindow();
            }
            else
            {
                Login currentAdUser = LoginAccess.Instance.GetCurrentUser();
                LoginAccess.Instance.FetchLogins();
                ActivityAccess.Instance.FetchActivityTypes();
                if (currentAdUser != null)
                {
                    TimerUtil.Instance.SetupTimers();
                    TimerUtil.Instance.StartTimers();
                }
            }

            // ThisAddIn_Shutdown is no longer raised, hence this event instead
            ((ApplicationEvents_11_Event)Application).Quit += ThisAddIn_Quit;
        }

        private void SetupCategories()
        {
            var categoryName = SharedVariables.Instance.ClosedCategory;

            Categories categories = Application.Session.Categories;
            if (!CategoryExists(categoryName))
                categories.Add(categoryName, OlCategoryColor.olCategoryColorPurple);
        }

        private bool CategoryExists(string categoryName)
        {
            try
            {
                Category category = Application.Session.Categories[categoryName];
                return category != null;
            }
            catch { return false; }
        }

        private void ThisAddIn_Quit()
        {
            TimerUtil.Instance.DisposeTimers();
        }

        private void SetMailItem(Inspector inspector)
        {
            var mailItem = inspector.CurrentItem as MailItem;
            if (mailItem != null)
                CurrentMailItem = mailItem;
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            // Note: Outlook no longer raises this event. If you have code that 
            //    must run when Outlook shuts down, see http://go.microsoft.com/fwlink/?LinkId=506785
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
