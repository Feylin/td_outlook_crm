﻿namespace FlexCRM.DTO
{
    public interface IContact
    {
        string  Phone   { get; set; }
        string  Title   { get; set; }
    }
}
