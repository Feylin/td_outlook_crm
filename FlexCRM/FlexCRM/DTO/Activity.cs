﻿using System;

namespace FlexCRM.DTO
{
    public class Activity
    {
        public long CompanyNo { get; set; }
        public long ActivityType { get; set; }
        public long Responsible { get; set; }
        public DateTime? FollowUp { get; set; }
        public string Headline { get; set; }
        public string Text { get; set; }
        public long Id { get; set; }
        public DateTime? Date { get; set; }

        protected bool Equals(Activity other)
        {
            return CompanyNo == other.CompanyNo && ActivityType == other.ActivityType 
                && Responsible == other.Responsible && FollowUp.Equals(other.FollowUp) 
                && string.Equals(Headline, other.Headline) && string.Equals(Text, other.Text) 
                && Id == other.Id && Date.Equals(other.Date);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Activity) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = CompanyNo.GetHashCode();
                hashCode = (hashCode*397) ^ ActivityType.GetHashCode();
                hashCode = (hashCode*397) ^ Responsible.GetHashCode();
                hashCode = (hashCode*397) ^ FollowUp.GetHashCode();
                hashCode = (hashCode*397) ^ (Headline != null ? Headline.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Text != null ? Text.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ Id.GetHashCode();
                hashCode = (hashCode*397) ^ Date.GetHashCode();
                return hashCode;
            }
        }
    }
}
