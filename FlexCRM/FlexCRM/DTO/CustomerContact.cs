﻿namespace FlexCRM.DTO
{
    public class CustomerContact : ModuleContact, IContact
    {
        //intarface members
        public string Phone { get; set; }
        public string Title { get; set; }

        //not
        public long CustomerContact_ID { get; set; }
    }
}
