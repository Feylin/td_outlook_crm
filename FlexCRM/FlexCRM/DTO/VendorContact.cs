﻿using System;
namespace FlexCRM.DTO
{
    public class VendorContact :ModuleContact, IContact
    {
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Title { get; set; }
        public long Module { get; set; }
        public string responsible { get; set; }
        public DateTime? Created { get; set; }
        public bool Active { get; set; }
        public long Vendor_ID { get; set; }
        public long VendorContact_ID { get; set; }
    }
}
