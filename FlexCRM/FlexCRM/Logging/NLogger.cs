﻿using System;
using NLog;

namespace FlexCRM.Logging
{
    public class NLogger
    {
        private static readonly Lazy<NLogger> Lazy = new Lazy<NLogger>(() => new NLogger());

        public static NLogger Instance = Lazy.Value;

        private NLogger()
        {
        }

        public Logger Logger => LogManager.GetCurrentClassLogger();
    }
}
