﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using FlexCRM.BLL;
using FlexCRM.DAL;
using FlexCRM.DAL.EF;
using FlexCRM.DTO;
using FlexCRM.Util;
using Microsoft.Office.Interop.Outlook;
using Exception = System.Exception;
using UserControl = System.Windows.Controls.UserControl;

namespace FlexCRM.GUI.StandAlone
{
    /// <summary>
    /// Interaction logic for SaveDocument.xaml
    /// </summary>
    public partial class SaveDocument : UserControl
    {
        private readonly MailAccess _mailAccess = new MailAccess();
        private readonly MailItem _mail;
        private readonly string _senderEmail;
        private List<ModuleContact> _contact;
        private RelayCommand _saveDocumentCommand;
        private ReadableGUIMessages _messageHandler;

        //can't put text directly in .xaml beacuse a string can't be converted to an IEnumerable by some type converter.
        public IEnumerable<char> ExpanderText =>"Create Activity";

        private ObservableCollection<DocModule> cmbModuleCollection; 

        public SaveDocument()
        {
            InitializeComponent();

            SetupMessageDisplayForChildren();

            //cmbModule.SelectedValue = Contact.Modul;
            _mail = ThisAddIn.CurrentMailItem;
            _senderEmail = _mailAccess.GetSenderSMTPAddress(_mail);

            PathsForComboBoxes();
            DatabaseCalls();
            
            btnSave.Command = SaveCommand;
            
            ViewCreateContact.ContactSaved += ContactSavedListener;
            ViewContactSelection.ContactSelected += ContactSelectedListener;
            ViewCreateCompany.CompanyCreated += CompanyCreatedListener;

            DocModulRef companyDo;

            if (_contact.Any())
                SetupContactFound();
            else if ((companyDo = CustomerAccess.Instance.FindDocModuleRefByDomain(_senderEmail))!= null)
            {
                SetCompany(companyDo);
                tbiContactKnown.IsEnabled = false;
                tbiCompany.IsSelected = true;
                tbiOther.IsSelected = false;
                SetupDomainSelection();
            }
            else
            {
                tbiContactKnown.IsEnabled = false;
                tbiCompany.IsSelected = false;
                tbiOther.IsSelected = true;
                SetupOtherSelection();
                ViewContactInfo.DisplayForNew(_senderEmail);
            }

        }

        private void CompanyCreatedListener(object sender, EventArgs args)
        {
            SetCompany(ViewCreateCompany.Company);
            ViewCreateContactCompany.SetCompany(ViewCreateCompany.Company);
            ViewCreateContact.SetCompany(ViewCreateCompany.Company);
        }

        private void SetupMessageDisplayForChildren()
        {
            _messageHandler = new ReadableGUIMessages((s => txbErrorStatus.Text = s));
            ViewCreateCompany.MessageHandler = _messageHandler;
            ViewCreateContact.MessageHandler = _messageHandler;
        }

        private void DatabaseCalls()
        {
            try
            {
                DocModuleAccess.Instance.GetDocModules().ForEach(mod => cmbModuleCollection.Add(mod));
                _contact = ContactAccess.Instance.GetContactByEmail(_senderEmail);
            }
            catch (Exception e)
            {
                if (!_messageHandler.TryHandle(e))
                    throw;
            }
        }

        private void SetCompany(DocModulRef company)
        {
            cmbCompany.ItemsSource = new [] {company};
            cmbCompany.SelectedValue = company.ID;
            cmbModule.SelectedValue = company.Modul;
            UpdatecmbStructure();
        }

        private ICommand SaveCommand
        {
            get
            {
                return _saveDocumentCommand ??
                       (_saveDocumentCommand =
                           new RelayCommand(param => btnSaveClicked(), param => btnSaveCanClick));
            }
        }

        private void PathsForComboBoxes()
        {
            cmbModule.SelectedValuePath = nameof(DocModule.ID);
            cmbModule.DisplayMemberPath = nameof(DocModule.DocModule1);
            cmbModule.ItemsSource = cmbModuleCollection = new ObservableCollection<DocModule>();

            cmbDocStructure.SelectedValuePath = nameof(DocStructure.ID);
            cmbDocStructure.DisplayMemberPath = nameof(DocStructure.Name);

            cmbCompany.SelectedValuePath = nameof(DocModulRef.ID);
            cmbCompany.DisplayMemberPath = nameof(DocModulRef.Name);
        }
        
        private bool btnSaveCanClick =>
            _contact != null && cmbModule.SelectedValue != null && cmbDocStructure.SelectedValue != null && cmbCompany.SelectedItem != null;

        private void btnSaveClicked()
        {
            DocModule module = ((DocModule)cmbModule.SelectedItem);
            DocStructure structure = (DocStructure)cmbDocStructure.SelectedItem;
            DocModulRef company = (DocModulRef)cmbCompany.SelectedItem;
           
            try
            {
                long? activityID = null;
                if (chkSaveActivity.IsEnabled && chkSaveActivity.IsChecked == true)
                {
                    activityID = ViewCreateActivity.SaveStuff();
                }
                _mailAccess.SaveEmailInDatabaseAndDirectory(_mail, module, company, structure, 
                                                            ViewCreateActivity.GetStartDate(), txtDescription.Text, activityID);

                Window.GetWindow(this)?.Close();
            }
            catch (Exception e)
            {
                if (!_messageHandler.TryHandle(e))
                    throw;
            }
        }

        //event callbacks
        private void ContactSavedListener(object sender, EventArgs args)
        {
            //selects the newly created contact
            _contact = new List<ModuleContact> {ViewContactInfo.Contact};
        }

        private void ContactSelectedListener(object sender, EventArgs args)
        {
            SetCompany(((ContactSelection)sender).Company);
            ViewCreateContact.SetCompany(((ContactSelection) sender).Company);
            ViewCreateContactCompany.SetCompany(((ContactSelection) sender).Company);
        }

        private void SetupDomainSelection()
        {
            tbiCompany.Background = Brushes.White;
            tbiCompany.Foreground = Brushes.Orange;
        }

        private void SetupOtherSelection()
        {
            tbiOther.Background = Brushes.White;
            tbiOther.Foreground = Brushes.Red;
            ViewContactInfo.ShowSaveButton(true);
        }

        /// <summary>
        /// a contact was found/or has been selected and the option exists to use that
        /// create a new or choose another existing contact
        /// </summary>
        /// <param name="contact"></param>
        public void SetupContactFound()
        {
            tbiContactKnown.Background = Brushes.White;
            tbiContactKnown.Foreground = Brushes.Green;
            tbiCompany.Background = Brushes.White;
            tbiCompany.Foreground = Brushes.Black;
            
            //configure child UserControl
            ViewContactInfo.DisplayContact(_contact[0]);
            ViewContactInfo.GrayContactFields(true);
            ViewContactInfo.ShowSaveButton(false);
            try
            {
                var refs = DocumentAccess.Instance.FindModuleRefsByContacts(_contact);
                cmbCompany.ItemsSource = refs;
                if (refs.Any())
                {
                    cmbCompany.SelectedValue = refs[0].ID;
                }
                cmbModule.ItemsSource = CustomerAccess.Instance.GetAllModul();
                cmbModule.SelectedValue = refs.FirstOrDefault()?.Modul;
                UpdatecmbStructure();
            }
            catch (Exception exe)
            {
                if (!_messageHandler.TryHandle(exe))
                    throw;
            }
        }

        public static void ShowWindow(Tuple<int,int> mouseXY)
        {
            var window = new Window
            {
                Left = mouseXY.Item1,
                Top = mouseXY.Item2,
                Content = new SaveDocument(),
                SizeToContent = SizeToContent.WidthAndHeight,
                ResizeMode = ResizeMode.NoResize
            };
            window.ShowDialog();
        }

        private void cmbModule_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                UpdatecmbStructure();
            }
            catch (Exception)
            {
                cmbDocStructure.ItemsSource = null;
            }
        }

        private void UpdatecmbStructure()
        {
            if (cmbModule.SelectedValue != null)
            {
                try
                {
                    var list = DocModuleAccess.Instance.StructureForModule((long)cmbModule.SelectedValue);
                    cmbDocStructure.ItemsSource = list;
                    if (list.Any())
                    {
                        cmbDocStructure.SelectedIndex = 0;
                    }
                }
                catch (Exception e)
                {
                    if (!_messageHandler.TryHandle(e))
                        throw;
                }
               
            }
        }
        
        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tbiContactKnown.IsSelected)
            {
                tbiOther.Background = Brushes.White;
                tbiOther.Foreground = Brushes.Black;
                tbiCompany.Background = Brushes.White;
                tbiCompany.Foreground = Brushes.Black;
            }

            else if (tbiCompany.IsSelected)
            {
                tbiOther.Background = Brushes.White;
                tbiOther.Foreground = Brushes.Black;
                tbiContactKnown.Background = Brushes.White;
                tbiContactKnown.Foreground = Brushes.Black;
            }
            else if (tbiOther.IsSelected)
            {
                tbiContactKnown.Background = Brushes.White;
                tbiContactKnown.Foreground = Brushes.Black;
                tbiCompany.Background = Brushes.White;
                tbiCompany.Foreground = Brushes.Black;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (Parent is Window)
                ((Window)Parent).Title = "CRM System";
        }
    }
}
