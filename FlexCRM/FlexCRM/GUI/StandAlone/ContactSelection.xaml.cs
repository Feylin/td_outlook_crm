﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using FlexCRM.DAL;
using FlexCRM.DAL.EF;
using FlexCRM.Util;

namespace FlexCRM.GUI.StandAlone
{

    /// <summary>
    /// Interaction logic for ContactSelection.xaml
    /// </summary>
    public partial class ContactSelection : UserControl
    {
        //hosted in a window or inside another element
        public DocModulRef Company { get; private set; }

        public delegate void ContactSelectedHandler(object sender, EventArgs args);
        public event ContactSelectedHandler ContactSelected;

        private RelayCommand _fileSaveCommand;

        public ContactSelection()
        {
            InitializeComponent();
            SearchList.DisplayMemberPath = nameof(DocModulRef.Name);
            SearchList.SelectedValuePath = nameof(DocModulRef.ID);
            btnOk.Command = SearchCommand;
        }

        private ICommand SearchCommand
        {
            get
            {
                return _fileSaveCommand ??
                       (_fileSaveCommand =
                           new RelayCommand(param => Search()));
            }
        }
        
        private void Search()
        {
            var companies = DocumentAccess.Instance.SearchModuleRefs(txtSearchField.Text);
            SearchList.ItemsSource = companies;
        }
        

        private void SearchList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Company = SearchList.SelectedItem as DocModulRef;
            if (Company != null)
            {
                ContactSelected?.Invoke(this,EventArgs.Empty);
            }
        }

        private void SearchList_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            Company = SearchList.Items[0] as DocModulRef;
            if (Company != null)
            {
                ContactSelected?.Invoke(this, EventArgs.Empty);
            }
        }

        private void txtSearchField_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btnOk_Click(object sender, System.Windows.RoutedEventArgs e)
        {

        }
    }
}