﻿using System.Windows.Controls;

namespace FlexCRM.GUI.StandAlone.TabSwitchContols
{
    /// <summary>
    /// Interaction logic for Automatic.xaml
    /// </summary>
    public partial class Automatic : UserControl
    {
        public Automatic()
        {
            InitializeComponent();
        }
    }
}
