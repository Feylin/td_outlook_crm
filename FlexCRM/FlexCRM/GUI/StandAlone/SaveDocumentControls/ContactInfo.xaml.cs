﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using FlexCRM.BLL;
using FlexCRM.DAL;
using FlexCRM.DAL.EF;
using FlexCRM.DTO;
using FlexCRM.Util;
using CustomerContact = FlexCRM.DTO.CustomerContact;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using TextBox = System.Windows.Controls.TextBox;
using UserControl = System.Windows.Controls.UserControl;
using VendorContact = FlexCRM.DTO.VendorContact;

namespace FlexCRM.GUI.StandAlone.SaveDocumentControls
{
    /// <summary>
    /// Interaction logic for ContactInfo.xaml.
    /// used by Save document.xaml.
    /// has callbacks to inform parent when a contact was saved.
    /// also used to display information of selected contact.
    /// </summary>
    public partial class ContactInfo : UserControl
    {
        //event for Informing SaveDocument about a new Contact being saved.
        public delegate void ContactSavedHandler(object sender, EventArgs args);
        public event ContactSavedHandler ContactSaved;
        private readonly MailAccess _mailAccess = new MailAccess();
        //variable to hold the saved contact or the contact chosen to be displayed
        public ModuleContact Contact { get; private set; }
        public ReadableGUIMessages MessageHandler { get; set; } = ReadableGUIMessages.DefaultMessageHandler;
        private bool SavedOnce = false;


        private enum DisplayMode
        {
            Other,
            Customer,
            Vendor
        };
        private DisplayMode Mode;

        private readonly ContactInfoViewModel _viewModel;
        //if a contact has been saved that contact will be displayed, un-editable and with the save button grayed out

        public ContactInfo()
        {
            InitializeComponent();
            SetComboboxPaths();
            _viewModel = new ContactInfoViewModel();

            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                try
                {
                    cmbModule.ItemsSource = CustomerAccess.Instance.GetAllModul();
                    cmbModule.SelectedValue = 1;//customer
                    Mode = DisplayMode.Customer;
                    UpdateGUIForCurrentMode();
                    btnSaveContact.Command = new RelayCommand(btnSaveContactClicked, btnSaveContactCanClick);
                }
                catch (Exception exe)
                {
                    if (!MessageHandler.TryHandle(exe))
                    {
                        throw;
                    }
                }
                
            }
        }

        
        private void SetComboboxPaths()
        {
            cmbCompany.SelectedValuePath = nameof(DocModulRef.ID);
            cmbCompany.DisplayMemberPath = nameof(DocModulRef.Name);

            cmbModule.SelectedValuePath = nameof(DocModule.ID);
            cmbModule.DisplayMemberPath = nameof(DocModule.DocModule1);
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                try
                {
                    txtEmail.Text = _mailAccess.GetSenderSMTPAddress(ThisAddIn.CurrentMailItem);
                }
                catch (Exception exe)
                {
                    if (!MessageHandler.TryHandle(exe))
                    {
                        throw;
                    }
                }
            }
        }

        private bool btnSaveContactCanClick(object obj)
        {
            return !SavedOnce 
                   && !string.IsNullOrWhiteSpace(txtEmail.Text)
                   && !string.IsNullOrWhiteSpace(txtName.Text)
                   //&& !string.IsNullOrWhiteSpace(txtPhone.Text)
                   && cmbCompany.SelectedItem != null
                   && cmbModule.SelectedItem != null;
        }

        /// <summary>
        /// changes displayed fields to match class of contact object.
        /// or if contact object is null the selected module.
        /// </summary>
        public void UpdateGUIForCurrentMode()
        {
            switch (Mode)
            {
                case DisplayMode.Customer:
                    txtTitle.Visibility = Visibility.Visible;
                    txtPhone.Visibility = Visibility.Visible;
                    txtMobile.Visibility = Visibility.Hidden;
                    break;
                case DisplayMode.Vendor:
                    txtMobile.Visibility = Visibility.Visible;
                    txtPhone.Visibility = Visibility.Visible;
                    txtTitle.Visibility = Visibility.Visible;
                    break;
                case DisplayMode.Other:
                    txtMobile.Visibility = Visibility.Hidden;
                    txtTitle.Visibility = Visibility.Hidden;
                    txtPhone.Visibility = Visibility.Hidden;
                    break;
            }
        }

        private void btnSaveContactClicked(object obj)
        {
            int ModuleId = (int)cmbModule.SelectedValue;
            try
            {
                switch (Mode)
                {
                    case DisplayMode.Customer:
                        Contact = ContactAccess.Instance.AddCustomerInformation(
                            txtEmail.Text,
                            txtName.Text,
                            txtPhone.Text,
                            (DocModulRef) cmbCompany.SelectedItem,
                            txtTitle.Text,
                            ModuleId);
                        break;
                    case DisplayMode.Vendor:
                        Contact = ContactAccess.Instance.AddVendorInformation(
                            txtEmail.Text,
                            txtName.Text,
                            txtPhone.Text,
                            (DocModulRef) cmbCompany.SelectedItem,
                            txtTitle.Text,
                            txtMobile.Text,
                            ModuleId);
                        break;
                    default:
                        Contact = ContactAccess.Instance.AddDocModuleRefInformation(
                            txtEmail.Text,
                            txtName.Text,
                            ((DocModulRef) cmbCompany.SelectedItem).No,
                            ModuleId);
                        break;
                }
                ContactSaved?.Invoke(this,EventArgs.Empty);
                lblFeedback.Content = "Contact Saved";
                SavedOnce = true;
            }
            catch (Exception exe)
            {
                if (!MessageHandler.TryHandle(exe))
                    throw;
            }
        }

        public void SetCompany(DocModulRef company)
        {
            cmbCompany.ItemsSource = new[] {company};
            cmbCompany.SelectedValue = company.ID;
        }

        public void DisplayContact(ModuleContact contact)
        {
            Contact = contact;

            DetermineDisplayModeFromContact();
            UpdateGUIForCurrentMode();
            cmbModule.SelectedValue = contact.Modul;
            txtEmail.Text = contact.Email;
            txtName.Text = contact.Name;

            Mode = DisplayMode.Other;

            try
            {
                var customerContact = contact as CustomerContact;
                if (customerContact != null)
                {
                    cmbCompany.ItemsSource=new[]{(DocumentAccess.Instance.FindCompanyByNoAndModule(customerContact.Company.Value, contact.Modul))} ;
                    cmbCompany.SelectedValue = customerContact.Company;
                    txtPhone.Text = customerContact.Phone;
                    txtTitle.Text = customerContact.Title;
                    Mode = DisplayMode.Customer;
                }
                var vendorContact = contact as VendorContact;
                if (vendorContact != null)
                {
                    cmbCompany.ItemsSource = new[] { (DocumentAccess.Instance.FindCompanyByNoAndModule(customerContact.Company.Value, contact.Modul)) };
                    cmbCompany.SelectedValue = customerContact.Company;
                    txtPhone.Text = vendorContact.Phone;
                    txtTitle.Text = vendorContact.Title;
                    txtMobile.Text = vendorContact.Mobile;
                    Mode = DisplayMode.Vendor;
                }
            }
            catch (Exception exe)
            {
                if (!MessageHandler.TryHandle(exe))
                    throw;
            }
            UpdateGUIForCurrentMode();
        }

        private void DetermineDisplayModeFromContact()
        {
            if(Contact == null)
                throw new ArgumentNullException();

            var type = Contact.GetType();

            if (type == typeof (CustomerContact))
                Mode = DisplayMode.Customer;
            else if (type == typeof (VendorContact))
                Mode = DisplayMode.Vendor;
            else if (type == typeof(ModuleContact))
                Mode = DisplayMode.Other;
        }

        /// <summary>
        /// makes the information that relates 
        /// directly to the contact un-editable
        /// </summary>
        public void GrayContactFields(bool gray)
        {
            cmbCompany.IsEditable = !gray;
            cmbModule.IsEditable = !gray;

            txtPhone.IsReadOnly = gray;
            txtEmail.IsReadOnly = gray;
            txtPhone.IsReadOnly = gray;
            txtName.IsReadOnly = gray;
            txtTitle.IsReadOnly = gray;
            
            cmbCompany.IsEnabled = !gray;
            cmbModule.IsEnabled = !gray;

            if (gray)
            {
                txtPhone.Background = new SolidColorBrush(Colors.DarkGray);
                txtEmail.Background = new SolidColorBrush(Colors.DarkGray);
                txtPhone.Background = new SolidColorBrush(Colors.DarkGray);
                txtName.Background = new SolidColorBrush(Colors.DarkGray);
                txtTitle.Background = new SolidColorBrush(Colors.DarkGray);
            }
            else
            {
                txtPhone.Background = new SolidColorBrush(Colors.White);
                txtEmail.Background = new SolidColorBrush(Colors.White);
                txtPhone.Background = new SolidColorBrush(Colors.White);
                txtName.Background = new SolidColorBrush(Colors.White);
                txtTitle.Background = new SolidColorBrush(Colors.White);
            }
        }

        public void DisplayForNew(string email)
        {
            txtEmail.Text = email;
        }
        
        public void ShowSaveButton(bool show)
        {
            if (show)
            {
                btnSaveContact.Visibility = Visibility.Visible;
            }
            else
            {
                btnSaveContact.Visibility = Visibility.Collapsed;
            }
        }

        //is set to true on key events other than Up and Down
        private bool ChangeOK;
        private void cmbCompany_TextInput(object sender, TextChangedEventArgs textChangedEventArgs)
        {
            if(ChangeOK)
            try
            {
                if (DisplayMode.Customer == Mode)
                    cmbCompany.ItemsSource = DocumentAccess.Instance.searchCompany(cmbCompany.Text,1);
                else if (DisplayMode.Vendor == Mode)
                    cmbCompany.ItemsSource = DocumentAccess.Instance.searchCompany(cmbCompany.Text, 2);
            }
            catch (Exception exe)
            {
                if (!MessageHandler.TryHandle(exe))
                    throw;
            }
            ChangeOK = false;
        }

        private void cmbCompany_KeyDown(object sender, KeyEventArgs e)
        {
            ChangeOK = e.Key != Key.Up || e.Key != Key.Down;
        }

        private void txtBox_LostFocus(object sender, RoutedEventArgs e)
        {
            var txtBox = sender as TextBox;

            if (txtBox != null)
                ValidateTextBox(txtBox);
        }

        private void ValidateTextBox(TextBox txtBox)
        {
            bool valid = false;
            switch (txtBox.Name)
            {
                case nameof(txtName):
                    valid = _viewModel.ValidateName(txtBox.Text);
                    break;
                case nameof(txtPhone):
                    valid = _viewModel.ValidatePhone(txtBox.Text);
                    break;
                case nameof(txtEmail):
                    valid = _viewModel.ValidateEmail(txtBox.Text);
                    break;
                case nameof(txtMobile):
                    valid = _viewModel.ValidatePhone(txtBox.Text);
                    break;
            }
            if (!valid)
                txtBox.BorderBrush = new SolidColorBrush(Colors.Red);
            else
                txtBox.BorderBrush = SystemColors.ActiveBorderBrush;
        }


        private void cmbModule_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbModule.SelectedValue == null)
                Mode = DisplayMode.Other;
            else if ((long)cmbModule.SelectedValue == 1) //customer
                Mode = DisplayMode.Customer;
            else if ((long)cmbModule.SelectedValue == 2) //vendor
                Mode = DisplayMode.Vendor;
            else
                Mode = DisplayMode.Other;
            UpdateGUIForCurrentMode();
        }

        private void cmbCompany_LostFocus(object sender, RoutedEventArgs e)
        {
            cmbCompany.IsDropDownOpen = false;
        }

        private void cmbCompany_GotFocus(object sender, RoutedEventArgs e)
        {
            cmbCompany.IsDropDownOpen = true;
        }

      
    }
}