﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;
using FlexCRM.DAL;
using FlexCRM.DAL.EF;
using FlexCRM.DTO;
using FlexCRM.Util;

namespace FlexCRM.GUI.StandAlone.SaveDocumentControls
{
    /// <summary>
    /// Interaction logic for CreateCompany.xaml
    /// </summary>
    public partial class CreateCompany : UserControl
    {
        public DocModulRef Company { get; private set; }
        
        public delegate void CompanyCreatedHandler(object sender, EventArgs args);
        public event CompanyCreatedHandler CompanyCreated;
        private bool CompanyCreatedFlag;

        public ReadableGUIMessages MessageHandler { get; set; } = ReadableGUIMessages.DefaultMessageHandler;

        private enum Mode
        {
            Customer,
            Vendor
        }
        private Mode DisplayMode = Mode.Customer;

        public CreateCompany()
        {
            InitializeComponent();
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                btnSaveCompany.Command = new RelayCommand(btnSaveCompanyClicked, btnSaveCompanyCanClick);
                cmbStatus.ItemsSource = CustomerAccess.Instance.FindAllStatuses();
                cmbGroup.ItemsSource = CustomerAccess.Instance.FindAllCustomerGroups();
                cmbResponsible.ItemsSource = LoginAccess.Instance.GetLogins().Values;

                setComboBoxPaths();

                cmbType.ItemsSource = new List<Tuple<string, long>>
                {
                    Tuple.Create("Customer",1L),
                    Tuple.Create("Vendor",2L)
                };

                cmbGroup.SelectionChanged += SetSubGroups;
                cmbType.SelectionChanged += AlterDisplay;
            }
        }

        private void setComboBoxPaths()
        {
            //name
            cmbGroup.DisplayMemberPath = nameof(Tuple<string, long>.Item1);
            //id
            cmbGroup.SelectedValuePath = nameof(Tuple<string, long>.Item2);

            cmbStatus.DisplayMemberPath = nameof(Tuple<string, long>.Item1);
            cmbStatus.SelectedValuePath = nameof(Tuple<string, long>.Item2);

            cmbType.DisplayMemberPath = nameof(Tuple<string, long>.Item1);
            cmbType.SelectedValuePath = nameof(Tuple<string, long>.Item2);

            cmbResponsible.DisplayMemberPath = nameof(User.Name);
            cmbResponsible.SelectedValuePath = nameof(User.Id);
        }

        private void AlterDisplay(object sender, SelectionChangedEventArgs args)
        {
            if (cmbType.SelectedItem != null)
            {
                switch ((long)cmbType.SelectedValue)
                {
                    case 1:
                        cmbSubgroup.Visibility = Visibility.Visible;
                        txtCountry.Visibility = Visibility.Visible;
                        txtVendorNo.Visibility = Visibility.Collapsed;
                        DisplayMode = Mode.Customer;
                        break;
                    case 2:
                        cmbSubgroup.Visibility = Visibility.Collapsed;
                        txtCountry.Visibility = Visibility.Collapsed;
                        txtVendorNo.Visibility = Visibility.Visible;
                        DisplayMode = Mode.Vendor;
                        break;
                }
            }
        }

        private void SetSubGroups(object sender, SelectionChangedEventArgs e)
        {
            if (cmbGroup.SelectedItem != null)
            {
                cmbSubgroup.ItemsSource = CustomerAccess.Instance.FindAllCustomerSubGroups((long)cmbGroup.SelectedValue);
            }
            e.Handled = false;
        }

        private bool btnSaveCompanyCanClick(object obj)
        {
            return cmbType.SelectedItem != null &&
               cmbGroup.SelectedItem != null &&
               !string.IsNullOrWhiteSpace(txtName.Text) &&
               (DisplayMode == Mode.Customer || !string.IsNullOrWhiteSpace(txtVendorNo.Text)) &&
               !CompanyCreatedFlag;
        }

        private void btnSaveCompanyClicked(object obj)
        {
            Customer cust = null;
            Vendor vend = null;
            switch ((long)cmbType.SelectedValue)
            {
                case 1: //TODO hardcoded value based on database ID :( (1 is customer)
                    cust = SaveCustomer();
                    break;
                case 2:
                    vend = SaveVendor();
                    break;
            }

            DocModulRef moduleRef = new DocModulRef();
            if (cust != null)
            {
                moduleRef.Modul = 1;
                moduleRef.Name = cust.Name;
                moduleRef.No = cust.ID;
            }
            else if (vend != null)
            {
                moduleRef.Modul = 2;
                moduleRef.Name = vend.Name;
                moduleRef.No = vend.ID;
            }
            else
            {
                return;
            }
            try
            {
                DocumentAccess.Instance.SaveModuleRef(moduleRef);
                try
                {
                    if(!string.IsNullOrWhiteSpace(txtDomain.Text))
                        CustomerAccess.Instance.AddCustomerDomains(txtDomain.Text, moduleRef.ID);
                }
                catch (Exception exe)
                {
#region stupidLongExceptionHandling
                    bool handled = false;
                    for (Exception inner = exe; inner != null; inner = inner.InnerException)
                    {
                        var sqlExe = inner as SqlException;
                        if(sqlExe==null) continue;
                        if (sqlExe.Number == 2627)
                        {
                            MessageHandler.ShowMessage($"Company Saved. Domain {txtDomain.Text} already referenced an existing company,\nno domain refence not added.");
                            handled = true;
                        }
                        else
                            throw;
                    }
                    if (!handled)
                        throw;
#endregion
                }
                Company = moduleRef;
                CompanyCreated?.Invoke(this,EventArgs.Empty);
            }
            catch (Exception exe)
            {
                if(!MessageHandler.TryHandle(exe))
                    throw;
            }
        }


        private Vendor SaveVendor()
        {
            Vendor vend = new Vendor
            {
                Name = txtName.Text,
                VendorGroup = (long) cmbGroup.SelectedValue,
                Status = (long?) cmbStatus.SelectedValue,
                Responsible = (long?) cmbResponsible.SelectedValue,
                Adress1 = string.IsNullOrWhiteSpace(txtRoadAndNR.Text) ? null : txtRoadAndNR.Text,
                City = string.IsNullOrWhiteSpace(txtCity.Text) ? null : txtCity.Text,
                VendorNo = txtVendorNo.Text
            };

            if (!string.IsNullOrWhiteSpace(txtZipCode.Text))
                try
                {
                    long zip = long.Parse(txtZipCode.Text);
                    vend.Zip = zip;
                }
                catch (FormatException)
                {
                    MessageHandler.ShowMessage("Zip code was malformed. A number was expected.");
                }

            try
            {
                VendorAccess.Instance.AddVendor(vend);
                ChangeToPostSaveState();
                return vend;
            }
            catch (Exception exe)
            {
                if (!MessageHandler.TryHandle(exe))
                    throw;
            }
            return null;
        }

        private Customer SaveCustomer()
        {
            Customer cust = new Customer
            {
                Name = txtName.Text,
                CustomerGroup = (long) cmbGroup.SelectedValue,
                Status = (long?) cmbStatus.SelectedValue,
                CustomerSubGroup = (long?) cmbSubgroup.SelectedValue,
                Adress1 = string.IsNullOrWhiteSpace(txtRoadAndNR.Text) ? null : txtRoadAndNR.Text,
                City = string.IsNullOrWhiteSpace(txtCity.Text) ? null : txtCity.Text,
                Zip = string.IsNullOrWhiteSpace(txtZipCode.Text) ? null : txtZipCode.Text,
                Country = string.IsNullOrWhiteSpace(txtCountry.Text) ? null : txtCountry.Text
            };

            try
            {
                CustomerAccess.Instance.AddNewCustomer(cust);
                ChangeToPostSaveState();
                return cust;
            }
            catch (Exception exe)
            {
                if (!MessageHandler.TryHandle(exe))
                    throw;
            }

            return null;
        }

        private void ChangeToPostSaveState()
        {
            CompanyCreatedFlag = true;
            btnSaveCompany.Visibility = Visibility.Collapsed;
            MessageHandler.ShowMessage($"{(DisplayMode == Mode.Customer ? "Customer" : "vendor")} saved to database");
        }
    }
}