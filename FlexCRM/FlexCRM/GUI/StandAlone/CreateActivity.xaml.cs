﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using FlexCRM.BLL;
using FlexCRM.DAL;
using FlexCRM.DAL.EF;
using FlexCRM.DTO;
using FlexCRM.Logging;
using FlexCRM.Util;

namespace FlexCRM.GUI.StandAlone
{
    /// <summary>
    /// Interaction logic for CreateActivity.xaml
    /// </summary>
    public partial class CreateActivity : UserControl
    {
        private bool _changeOK;
        private readonly CalendarViewModel _cvm;
        private readonly IActivityAccess _activityAccess = ActivityAccess.Instance;
        private readonly ILoginAccess _loginAccess = LoginAccess.Instance;
        private readonly CustomerAccess _customerAccess = CustomerAccess.Instance;
        private readonly List<string> _dateShortString = new List<string>();
        private RelayCommand _saveAndCloseCommand;
        private int _previousStartDateIndex;
        private string _windowTitle = "Create Activity";
        private long? _activityID = null;

        public static readonly DependencyProperty IsOwnWindowProperty = DependencyProperty.Register("IsOwnWindow", typeof(bool),
                        typeof(CreateActivity), new FrameworkPropertyMetadata(true, IsOwnWindowChanged));

        public static readonly DependencyProperty CanSaveProperty = DependencyProperty.Register("CanSave", typeof(bool),
                        typeof(CreateActivity), new FrameworkPropertyMetadata(false));

        public bool CanSave
        {
            get { return (bool) GetValue(CanSaveProperty); } 
            set { SetValue(CanSaveProperty, value); }
        }

        private static void IsOwnWindowChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            CreateActivity This = source as CreateActivity;

            if ((bool) e.NewValue)
            {
                This.btnSaveAndClose.Visibility = Visibility.Visible;
                This.btnSaveAndClose.IsEnabled = true;
            }
            else
            {
                This.btnSaveAndClose.Visibility = Visibility.Collapsed;
                This.btnSaveAndClose.IsEnabled = false;
            }
        }

        public bool IsOwnWindow
        {
            get { return (bool) GetValue(IsOwnWindowProperty); }
            set { SetValue(IsOwnWindowProperty, value); } 
        } 

        public CreateActivity()
        {
            InitializeComponent();

            var dateTime = DateTime.Today;

            for (var time = dateTime; time < dateTime.AddHours(24); time = time.AddMinutes(30))
                _dateShortString.Add(time.ToShortTimeString());

            cmbFollowupDate.ItemsSource = _dateShortString;

            cmbFollowupDate.SelectedIndex = 0;
            cmbReminder.SelectedIndex = 0;

            dpFollowupDate.SelectedDate = dateTime;
            _previousStartDateIndex = 0;

            cmbType.DisplayMemberPath = nameof(CustomerActivityType.ActivityType);
            cmbType.SelectedValuePath = nameof(CustomerActivityType.ID);

            cmbResponsible.DisplayMemberPath = nameof(User.Name);
            cmbResponsible.SelectedValuePath = nameof(User.Id);

            // Customer name
            cmbCustomer.DisplayMemberPath = nameof(Tuple<string, long>.Item1);
            // Customer id
            cmbCustomer.SelectedValuePath = nameof(Tuple<string, long>.Item2);

            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                // Database calls
                cmbType.ItemsSource = _activityAccess.GetActivityTypes().Values;
                cmbResponsible.ItemsSource = _loginAccess.GetLogins().Values;
                _cvm = CalendarViewModel.Instance;
            }
            cmbReminder.ItemsSource = Reminders();
            btnSaveAndClose.Command = SaveAndCloseCommand;
        }

        private ICommand SaveAndCloseCommand
        {
            get
            {
                return _saveAndCloseCommand ??
                       (_saveAndCloseCommand =
                           new RelayCommand(param => SaveAndCloseExecuted(), param => SaveAndCloseCanExecute));
            }
        }

        private void SaveAndCloseExecuted()
        {
            int? reminder = null;
            DateTime followUpDate = dpFollowupDate.SelectedDate.GetValueOrDefault().Add(TimeSpan.Parse(cmbFollowupDate.Text));

            var responsibleId = (long) cmbResponsible.SelectedValue;
            var currentUser = _loginAccess.GetCurrentUser();

            CustomerActivity cusActivity = new CustomerActivity
            {
                Type = (long) cmbType.SelectedValue,
                Customer = (long) cmbCustomer.SelectedValue,
                Responsible = responsibleId,
                Headline = txtSubject.Text,
                Text = txtMessage.Text,
                Date = DateTime.Now,
                Followup = followUpDate
            };

            if (cmbReminder.SelectedIndex > 0)
                reminder = (int?) ConvertSelectedReminderToMinutes(cmbReminder.Text);

            try
            {
                _activityID = _activityAccess.SaveActivity(cusActivity);
                if (responsibleId == currentUser.ID)
                    _cvm.CreateAppointment(cusActivity, currentUser, reminder);
                if (IsOwnWindow)
                    Window.GetWindow(this)?.Close();
            }
            catch (Exception ex)
            {
                NLogger.Instance.Logger.Error(ex);
                MessageBox.Show("An error occured while saving the activity", _windowTitle, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private List<string> Reminders()
        {
            var times = new List<string>
            {
                "None",
                TimeSpan.FromMinutes(5).ToReadableString(),
                TimeSpan.FromMinutes(10).ToReadableString(),
                TimeSpan.FromMinutes(15).ToReadableString(),
                TimeSpan.FromMinutes(30).ToReadableString(),
                TimeSpan.FromHours(1).ToReadableString(),
                TimeSpan.FromHours(2).ToReadableString(),
                TimeSpan.FromHours(3).ToReadableString(),
                TimeSpan.FromHours(4).ToReadableString(),
                TimeSpan.FromHours(5).ToReadableString(),
                TimeSpan.FromHours(6).ToReadableString(),
                TimeSpan.FromHours(7).ToReadableString(),
                TimeSpan.FromHours(8).ToReadableString(),
                TimeSpan.FromHours(9).ToReadableString(),
                TimeSpan.FromHours(10).ToReadableString(),
                TimeSpan.FromHours(11).ToReadableString(),
                TimeSpan.FromHours(12).ToReadableString(),
                TimeSpan.FromHours(18).ToReadableString(),
                TimeSpan.FromDays(1).ToReadableString(),
                TimeSpan.FromDays(2).ToReadableString(),
                TimeSpan.FromDays(3).ToReadableString(),
                TimeSpan.FromDays(4).ToReadableString()
            };

            return times;
        }

        private double ConvertSelectedReminderToMinutes(string selectedText)
        {
            string[] parts = selectedText.Split(' ');
            double timeAmount = Convert.ToInt32(parts[0]);
            string timeQuantifier = parts[1];

            if (timeQuantifier.Contains("minute"))
                return timeAmount;
            if (timeQuantifier.Contains("hour"))
                return TimeSpan.FromHours(timeAmount).TotalMinutes;
            return timeQuantifier.Contains("day") ? TimeSpan.FromDays(timeAmount).TotalMinutes : 0;
        }

        public bool SaveAndCloseCanExecute 
        {
            get {
                CanSave = !string.IsNullOrWhiteSpace(txtSubject.Text) && !string.IsNullOrWhiteSpace(txtMessage.Text)
                    && cmbType.SelectedItem != null && cmbResponsible.SelectedItem != null && cmbCustomer.SelectedItem != null;
                return CanSave;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (Parent is Window)
                ((Window)Parent).Title = _windowTitle;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            _previousStartDateIndex = cmbFollowupDate.SelectedIndex;

            cmbFollowupDate.IsEnabled = false;

            cmbFollowupDate.SelectedIndex = 0;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            cmbFollowupDate.IsEnabled = true;

            cmbFollowupDate.SelectedIndex = _previousStartDateIndex;

            if (_previousStartDateIndex == 0)
                cmbFollowupDate.SelectedIndex = 0;
        }

        public DateTime? GetStartDate()
        {
            if (SaveAndCloseCanExecute)
                return dpFollowupDate.SelectedDate.GetValueOrDefault().Add(TimeSpan.Parse(cmbFollowupDate.Text));
            return null;
        }

        public long? SaveStuff()
        {
            SaveAndCloseExecuted();
            return _activityID;
        }

        private void cmbCustomer_TextInput(object sender, TextChangedEventArgs textChangedEventArgs)
        {
            if (_changeOK)
            {
                if (cmbCustomer.Text.Length > 1)
                    cmbCustomer.ItemsSource = _customerAccess.SearchCompany(cmbCustomer.Text, 10);
                if (string.IsNullOrWhiteSpace(cmbCustomer.Text))
                    cmbCustomer.ItemsSource = _customerAccess.SearchCompany(string.Empty, 10);
            }
            _changeOK = false;
        }

        private void cmbCustomer_GotFocus(object sender, RoutedEventArgs e)
        {
            if (cmbCustomer.ItemsSource == null)
                cmbCustomer.ItemsSource = _customerAccess.SearchCompany(string.Empty, 10);
            cmbCustomer.IsDropDownOpen = true;
        }

        private void cmbCustomer_LostFocus(object sender, RoutedEventArgs e)
        {
            cmbCustomer.IsDropDownOpen = false;
        }

        private void cmbCustomer_KeyDown(object sender, KeyEventArgs e)
        {
            _changeOK = e.Key != Key.Up || e.Key != Key.Down;
        }
    }
}
