﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using FlexCRM.BLL;
using FlexCRM.DAL;
using FlexCRM.Util;
using MessageBox = System.Windows.MessageBox;
using UserControl = System.Windows.Controls.UserControl;

namespace FlexCRM.GUI.StandAlone
{
    /// <summary>
    /// Interaction logic for DatabaseConnection.xaml
    /// </summary>
    public partial class DatabaseConnection : UserControl
    {
        private readonly string _previousRegistryValue;
        private readonly Dictionary<string, string> _defaultTextDictionary;
        private RelayCommand _testConnectionCommand;
        private RelayCommand _saveConnectionCommand;
        private bool _connectionPassed;
        private readonly ReadableGUIMessages _messageHandler;

        public static void CreateWindow()
        {
            var window = new Window
            {
                Content = new DatabaseConnection(),
                SizeToContent = SizeToContent.WidthAndHeight,
                ResizeMode = ResizeMode.NoResize
            };
            window.ShowDialog();
        }

        private DatabaseConnection()
        {
            InitializeComponent();

            _previousRegistryValue = SettingsHandler.Instance.ReadConfig();

            btnTest.Command = TestCommand;
            btnSave.Command = SaveCommand;

            _defaultTextDictionary = new Dictionary<string, string>
            {
                {txtServer.Name, "Please enter a database server"},
                {txtDatabase.Name, "Please enter a database name"},
                {txtUser.Name, "Please enter a username"}
            };

            _messageHandler = new ReadableGUIMessages(ShowSqlError);
        }

        private ICommand TestCommand
        {
            get
            {
                return _testConnectionCommand ??
                       (_testConnectionCommand =
                           new RelayCommand(param => TestConnectionExecuted(), param => TestConnectionCanExecute));
            }
        }

        public bool TestConnectionCanExecute => 
            !(string.IsNullOrWhiteSpace(txtDatabase.Text) || txtDatabase.Text == _defaultTextDictionary[txtDatabase.Name]) && 
            !(string.IsNullOrWhiteSpace(txtServer.Text) || txtServer.Text == _defaultTextDictionary[txtServer.Name]) &&
            ((
            !(string.IsNullOrWhiteSpace(txtUser.Text) || txtUser.Text == _defaultTextDictionary[txtUser.Name]) &&
            !string.IsNullOrWhiteSpace(pwbPassword.Password))|| 
            chkIntegratedSecurity.IsChecked.GetValueOrDefault());

        private void TestConnectionExecuted()
        {
            try
            {
                if (TestConnection())
                {
                    MessageBox.Show("Database connection successfully established.", "Database connection",
                        MessageBoxButton.OK, MessageBoxImage.Information);
                    _connectionPassed = true;
                }
            }
            catch (SqlException ex)
            {
                _messageHandler.TryHandle(ex);
            }
        }

        private void ShowSqlError(string message)
        {
            MessageBox.Show(message, "Database connection", MessageBoxButton.OK, MessageBoxImage.Error);
            _connectionPassed = false;
        }

        private ICommand SaveCommand
        {
            get
            {
                return _saveConnectionCommand ??
                       (_saveConnectionCommand =
                           new RelayCommand(param => SaveConnectionExecuted(), param => SaveConnectionCanExecute));
            }
        }

        public bool SaveConnectionCanExecute => _connectionPassed;

        private void SaveConnectionExecuted()
        {
            if (SaveConnection())
            {
                if (string.IsNullOrWhiteSpace(_previousRegistryValue))
                {
                    LoginAccess.Instance.FetchLogins();
                    ActivityAccess.Instance.FetchActivityTypes();
                    TimerUtil.Instance.SetupTimers();
                    TimerUtil.Instance.StartTimers();
                }

                //"reset" DBContextHandler
                DBContextHandler.ZeroConnectionString();
                MessageBox.Show("Database connection successfully established and saved.", "Connected to database", MessageBoxButton.OK, MessageBoxImage.Information);
                Window parentWindow = Parent as Window;
                if (parentWindow != null)
                {
                    parentWindow.DialogResult = true;
                    parentWindow.Close();
                }
            }
            else
            {
                MessageBox.Show("Saving failed. please recheck the entered information", "Connection timed out", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool TestConnection()
        {
            return SettingsParser.Instance.TestDatabaseConnection(
                server: txtServer.Text,
                db: txtDatabase.Text,
                user: txtUser.Text,
                pw: pwbPassword.Password,
                integratedSecurity: chkIntegratedSecurity.IsChecked.GetValueOrDefault());
        }

        private bool SaveConnection()
        {
            bool integratedSecurityChecked = chkIntegratedSecurity.IsChecked.GetValueOrDefault();

            return SettingsParser.Instance.SaveDatabaseConnection(
                server: txtServer.Text,
                db: txtDatabase.Text,
                user: integratedSecurityChecked ? string.Empty : txtUser.Text,
                pw: integratedSecurityChecked ? string.Empty : pwbPassword.Password,
                integratedSecurity: integratedSecurityChecked);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SetInitialValues();

            if (Parent is Window)
            {
                ((Window)Parent).Title = "Database Settings";
            }
        }

        private void SetInitialValues()
        {
            var currentConnectionInfo = SettingsHandler.Instance.GetConnectionInfo();
            var sharedVariables = SharedVariables.Instance;

            SetTextboxText(txtServer, currentConnectionInfo[sharedVariables.Server]);
            SetTextboxText(txtDatabase, currentConnectionInfo[sharedVariables.Database]);
            SetTextboxText(txtUser, currentConnectionInfo[sharedVariables.User]);
        }

        private void SetTextboxText(TextBox txtBox, string connectionData)
        {
            if (string.IsNullOrWhiteSpace(connectionData))
                txtBox.Text = _defaultTextDictionary[txtBox.Name];
            else
                txtBox.Text = connectionData;
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            var txtBox = sender as TextBox;

            if (txtBox != null && string.IsNullOrWhiteSpace(txtBox.Text))
            {
                txtBox.Text = _defaultTextDictionary[txtBox.Name];
            }
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            var txtBox = sender as TextBox;

            if (txtBox != null && txtBox.Text.Equals(_defaultTextDictionary[txtBox.Name]))
            {
                txtBox.Text = string.Empty;
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            _connectionPassed = false;
        }

        private void pwbPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            _connectionPassed = false;
        }

        private void chkIntegratedSecurity_Checked(object sender, RoutedEventArgs e)
        {
            _connectionPassed = false;

            var chkBox = sender as CheckBox;

            if (chkBox != null)
            {
                switch (chkBox.IsChecked.GetValueOrDefault())
                {
                    case true:
                        txtUser.IsEnabled = false;
                        pwbPassword.IsEnabled = false;
                        break;
                    case false:
                        txtUser.IsEnabled = true;
                        pwbPassword.IsEnabled = true;
                        break;
                }
            }
        }
    }
}
