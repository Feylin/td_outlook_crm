﻿using System.Windows;
using System.Windows.Controls;
using FlexCRM.BLL;

namespace FlexCRM.GUI.StandAlone
{
    /// <summary>
    /// Interaction logic for EmailList.xaml
    /// </summary>
    public partial class EmailList : UserControl
    {
        private EmailSelectionViewModel viewModel;
        public EmailList()
        {
            viewModel = new EmailSelectionViewModel();
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            var companies = viewModel.Search(txtSearchField.Text);
            listEmailBox.ItemsSource = companies;
        }
    }
}
