﻿namespace FlexCRM
{
    partial class SaveEmailCRM : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SaveEmailCRM()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.btnSaveInDBAndServerCon = this.Factory.CreateRibbonButton();
            this.btnOtherSave = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.group1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.Groups.Add(this.group1);
            this.tab1.Label = "Flex CRM";
            this.tab1.Name = "tab1";
            // 
            // group1
            // 
            this.group1.Items.Add(this.btnSaveInDBAndServerCon);
            this.group1.Items.Add(this.btnOtherSave);
            this.group1.Label = "Email";
            this.group1.Name = "group1";
            // 
            // btnSaveInDBAndServerCon
            // 
            this.btnSaveInDBAndServerCon.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnSaveInDBAndServerCon.Label = "Save Email";
            this.btnSaveInDBAndServerCon.Name = "btnSaveInDBAndServerCon";
            this.btnSaveInDBAndServerCon.OfficeImageId = "RecordsSaveAsOutlookContact";
            this.btnSaveInDBAndServerCon.ShowImage = true;
            this.btnSaveInDBAndServerCon.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSaveInDBAndServerCon_Click);
            // 
            // btnOtherSave
            // 
            this.btnOtherSave.Label = "";
            this.btnOtherSave.Name = "btnOtherSave";
            // 
            // SaveEmailCRM
            // 
            this.Name = "SaveEmailCRM";
            this.RibbonType = "Microsoft.Outlook.Mail.Compose, Microsoft.Outlook.Mail.Read, Microsoft.Outlook.Po" +
    "st.Compose, Microsoft.Outlook.Post.Read";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon1_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSaveInDBAndServerCon;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnOtherSave;
    }

    partial class ThisRibbonCollection
    {
        internal SaveEmailCRM Ribbon1
        {
            get { return this.GetRibbon<SaveEmailCRM>(); }
        }
    }
}
