﻿namespace FlexCRM
{
    partial class MainCRM : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public MainCRM()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.menu1 = this.Factory.CreateRibbonMenu();
            this.btnImportCalendar = this.Factory.CreateRibbonButton();
            this.btnSynchronizeCalendar = this.Factory.CreateRibbonButton();
            this.btnRemoveCalendar = this.Factory.CreateRibbonButton();
            this.btnCreateActivity = this.Factory.CreateRibbonButton();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.btnSetDBCon = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.group2.SuspendLayout();
            this.group1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.Groups.Add(this.group2);
            this.tab1.Groups.Add(this.group1);
            this.tab1.Label = "Flex CRM";
            this.tab1.Name = "tab1";
            // 
            // group2
            // 
            this.group2.Items.Add(this.menu1);
            this.group2.Items.Add(this.btnCreateActivity);
            this.group2.Label = "Calendar";
            this.group2.Name = "group2";
            // 
            // menu1
            // 
            this.menu1.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.menu1.Items.Add(this.btnImportCalendar);
            this.menu1.Items.Add(this.btnSynchronizeCalendar);
            this.menu1.Items.Add(this.btnRemoveCalendar);
            this.menu1.Label = "Calendar";
            this.menu1.Name = "menu1";
            this.menu1.OfficeImageId = "CalendarHorizontal";
            this.menu1.ScreenTip = "Calendar Functions";
            this.menu1.ShowImage = true;
            this.menu1.SuperTip = "Main menu for calendar functions.";
            // 
            // btnImportCalendar
            // 
            this.btnImportCalendar.Label = "Import";
            this.btnImportCalendar.Name = "btnImportCalendar";
            this.btnImportCalendar.OfficeImageId = "Import";
            this.btnImportCalendar.ScreenTip = "Import appointsments";
            this.btnImportCalendar.ShowImage = true;
            this.btnImportCalendar.SuperTip = "Imports appointments from the database to the FlexCRM calendar.";
            this.btnImportCalendar.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnImportCalendar_Click);
            // 
            // btnSynchronizeCalendar
            // 
            this.btnSynchronizeCalendar.Label = "Synchronize";
            this.btnSynchronizeCalendar.Name = "btnSynchronizeCalendar";
            this.btnSynchronizeCalendar.OfficeImageId = "OpenInNewWindowSiteClient";
            this.btnSynchronizeCalendar.ScreenTip = "Synchronize calendar";
            this.btnSynchronizeCalendar.ShowImage = true;
            this.btnSynchronizeCalendar.SuperTip = "Perform synchronization between the calendar appointments and the appointments in" +
    " the database";
            this.btnSynchronizeCalendar.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSynchronizeCalendar_Click);
            // 
            // btnRemoveCalendar
            // 
            this.btnRemoveCalendar.Label = "Remove";
            this.btnRemoveCalendar.Name = "btnRemoveCalendar";
            this.btnRemoveCalendar.OfficeImageId = "RemoveFromCalendar";
            this.btnRemoveCalendar.ScreenTip = "Remove calendar";
            this.btnRemoveCalendar.ShowImage = true;
            this.btnRemoveCalendar.SuperTip = "Performs cleanup of the calendar by removing all Flex appointments";
            this.btnRemoveCalendar.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnRemoveCalendar_Click);
            // 
            // btnCreateActivity
            // 
            this.btnCreateActivity.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnCreateActivity.Label = "Create Activity";
            this.btnCreateActivity.Name = "btnCreateActivity";
            this.btnCreateActivity.OfficeImageId = "AssignTask";
            this.btnCreateActivity.ScreenTip = "Create a FlexCRM activity.";
            this.btnCreateActivity.ShowImage = true;
            this.btnCreateActivity.SuperTip = "Displays a dialog that enables craetion and saving of an activity.";
            this.btnCreateActivity.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnCreateActivity_Click);
            // 
            // group1
            // 
            this.group1.Items.Add(this.btnSetDBCon);
            this.group1.Label = "Settings";
            this.group1.Name = "group1";
            // 
            // btnSetDBCon
            // 
            this.btnSetDBCon.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnSetDBCon.Label = "Edit Database Settings";
            this.btnSetDBCon.Name = "btnSetDBCon";
            this.btnSetDBCon.OfficeImageId = "FileCompactAndRepairDatabase";
            this.btnSetDBCon.ScreenTip = "Modify Database Connection";
            this.btnSetDBCon.ShowImage = true;
            this.btnSetDBCon.SuperTip = "Displays a dialog that allows for modification of the database connection.";
            this.btnSetDBCon.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSetDBCon_Click);
            // 
            // MainCRM
            // 
            this.Name = "MainCRM";
            this.RibbonType = "Microsoft.Outlook.Explorer";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.MainCRM_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSetDBCon;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnImportCalendar;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSynchronizeCalendar;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnRemoveCalendar;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnCreateActivity;
    }

    partial class ThisRibbonCollection
    {
        internal MainCRM MainCRM
        {
            get { return this.GetRibbon<MainCRM>(); }
        }
    }
}
