﻿namespace FlexCRM
{
    partial class ActivityRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ActivityRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.btnCloseActivity = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.group1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.Groups.Add(this.group1);
            this.tab1.Label = "Flex CRM";
            this.tab1.Name = "tab1";
            // 
            // group1
            // 
            this.group1.Items.Add(this.btnCloseActivity);
            this.group1.Label = "Activity";
            this.group1.Name = "group1";
            // 
            // btnCloseActivity
            // 
            this.btnCloseActivity.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnCloseActivity.Label = "Close Activity";
            this.btnCloseActivity.Name = "btnCloseActivity";
            this.btnCloseActivity.OfficeImageId = "FileDocumentEncrypt";
            this.btnCloseActivity.ScreenTip = "Close Activity";
            this.btnCloseActivity.ShowImage = true;
            this.btnCloseActivity.SuperTip = "Closes the selected activity";
            this.btnCloseActivity.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnCloseActivity_Click);
            // 
            // ActivityRibbon
            // 
            this.Name = "ActivityRibbon";
            this.RibbonType = "Microsoft.Outlook.Appointment";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.ActivityRibbon_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnCloseActivity;
    }

    partial class ThisRibbonCollection
    {
        internal ActivityRibbon ActivityRibbon
        {
            get { return this.GetRibbon<ActivityRibbon>(); }
        }
    }
}
