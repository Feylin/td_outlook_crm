﻿using System;
using Microsoft.Office.Tools.Ribbon;
using FlexCRM.GUI.StandAlone;
using FlexCRM.Util;

namespace FlexCRM
{
    public partial class SaveEmailCRM
    {
        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void btnSaveInDBAndServerCon_Click(object sender, RibbonControlEventArgs e)
        {
            //if exceptions aren't handled they should be thrown not just quitly die in an empty catch block
            Tuple<int, int> coordXY = GetMousePosition.GetMousePositionMethod();
            SaveDocument.ShowWindow(coordXY);
        }
    }
}
