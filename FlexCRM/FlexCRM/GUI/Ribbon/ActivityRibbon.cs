﻿using FlexCRM.DAL;
using FlexCRM.Util;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Tools.Ribbon;

namespace FlexCRM
{
    public partial class ActivityRibbon
    {
        private readonly IActivityAccess _activityAccess = ActivityAccess.Instance;

        private void ActivityRibbon_Load(object sender, RibbonUIEventArgs e)
        {

        }

        // It is only possible to close flex activities, which means they have be present in the database
        // If this should be changed, remove the activity reference and check 'appItem' for null instead
        private void btnCloseActivity_Click(object sender, RibbonControlEventArgs e)
        {
            if (Globals.ThisAddIn.Application.ActiveExplorer().Selection.Count > 0)
            {
                object selObject = Globals.ThisAddIn.Application.ActiveExplorer().Selection[1];
                if (selObject is AppointmentItem)
                {
                    AppointmentItem apptItem = selObject as AppointmentItem;
                    var activity = _activityAccess.GetActivityById((long)apptItem.UserProperties.Find("aId").Value);
                    if (activity != null)
                    {
                        if (apptItem.Categories == null) // Close activity
                        {
                            apptItem.Categories = SharedVariables.Instance.ClosedCategory;
                            apptItem.Save();
                        }
                        //else // Reopen activity
                        //{
                        //    apptItem.Categories = null;
                        //    apptItem.Save();
                        //}
                    }
                }
            }
        }
    }
}
