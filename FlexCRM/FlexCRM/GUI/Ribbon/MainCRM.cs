﻿using System;
using System.Threading.Tasks;
using Microsoft.Office.Tools.Ribbon;
using System.Windows;
using FlexCRM.GUI.StandAlone;
using FlexCRM.BLL;
using FlexCRM.DAL;
using FlexCRM.Util;

namespace FlexCRM
{
    public partial class MainCRM
    {
        private CalendarViewModel _cvm;

        private void MainCRM_Load(object sender, RibbonUIEventArgs e)
        {
            if (SettingsHandler.Instance.DatabaseSetup())
                _cvm = CalendarViewModel.Instance;
        }

        private void btnSetDBCon_Click(object sender, RibbonControlEventArgs e)
        {
            DatabaseConnection.CreateWindow();
        }

        private void btnImportCalendar_Click(object sender, RibbonControlEventArgs e)
        {
            ValidDatabaseConnectionCheck(() => Task.Run(() => _cvm.ImportCalendar()));
        }

        private void btnSynchronizeCalendar_Click(object sender, RibbonControlEventArgs e)
        {
            ValidDatabaseConnectionCheck(() => Task.Run(() => _cvm.SynchronizeCalendar()));
        }

        private void btnRemoveCalendar_Click(object sender, RibbonControlEventArgs e)
        {
            ValidDatabaseConnectionCheck(() => Task.Run(() => _cvm.RemoveAllFlexAppointments()));
        }

        private void btnCreateActivity_Click(object sender, RibbonControlEventArgs e)
        {
            ValidDatabaseConnectionCheck(() => _cvm.CreateActivity());
        }

        private void ValidDatabaseConnectionCheck(Action action)
        {
            if (!SettingsHandler.Instance.DatabaseSetup())
            {
                MessageBoxResult result = MessageBox.Show("Calendar functions will not work without a valid database connection.\n" +
                                "Setup a connection now?", "FlexCRM Calendar", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                if (result == MessageBoxResult.Yes)
                    DatabaseConnection.CreateWindow();
            }
            if (LoginAccess.Instance.GetCurrentUser() == null)
            {
                MessageBox.Show("No matching active directory user found, calendar functions disabled.", "FlexCRM Calendar", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                if (_cvm == null)
                    _cvm = CalendarViewModel.Instance;
                TimerUtil.Instance.SetupTimers();
                TimerUtil.Instance.StartTimers();
                action.Invoke();
            }
        }
    }
}
