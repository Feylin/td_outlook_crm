﻿using System;
using System.Runtime.InteropServices;

namespace FlexCRM.Util
{
    public class GetMousePosition
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetCursorPos(ref Win32Point pt);

        [StructLayout(LayoutKind.Sequential)]
        internal struct Win32Point
        {
            public Int32 X;
            public Int32 Y;
        };

        /// <summary>
        /// gets the mouse position in absolute pixel value NOT device independant units
        /// so fields expecting device independent units will become inacurate the more scaling 
        /// and further from the upper left corner
        /// </summary>
        /// <returns></returns>
        public static Tuple<int,int> GetMousePositionMethod()
        {
            Win32Point w32Mouse = new Win32Point();
            GetCursorPos(ref w32Mouse);
            return  Tuple.Create(w32Mouse.X, w32Mouse.Y);
        }
    }
}
