﻿using System;

namespace FlexCRM.Util
{
    public class FlexCRMException : Exception
    {
        public FlexCRMException(string msg) : base(msg)
        {
            
        }
    }
}
