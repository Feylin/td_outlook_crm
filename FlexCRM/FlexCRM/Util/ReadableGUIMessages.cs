﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FlexCRM.Util
{
    public class ReadableGUIMessages
    {
        private Action<string> _writeMessage; 

        public static readonly ReadableGUIMessages DefaultMessageHandler = new ReadableGUIMessages(s =>
        {
            MessageBox.Show(s, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        });

        public ReadableGUIMessages(Action<string> writeMessage)
        {
            _writeMessage = writeMessage;
        }

        /// <summary>
        /// "handles" exception by showing a message.
        /// matches the exception against a number of exception types and property values displays a message for certain exceptions
        /// </summary>
        /// <param name="exception"></param>
        /// <returns>returns false if it had no message to display for the parameter "exception"</returns>
        public bool TryHandle(Exception exception)
        {
            bool handled = false;
            if (exception is FlexCRMException)
            {
                _writeMessage(exception.Message);
                handled = true;
            }
            else for (Exception inner = exception; inner != null; inner = inner.InnerException)
            {
                SqlException sqlException = inner as SqlException;
                if (sqlException != null)
                {
                    _writeMessage(GetReadableErrorMessage(sqlException));
                    handled = true;
                }
            }
            return handled;
        }

        private string GetReadableErrorMessage(SqlException exception)
        {
            switch (exception.Number)
            {
                case -2:
                    return "The timeout period elapsed prior to completion of the operation or the server is not responding";
                case -1: // Format error
                    return "Error location server/instance specified\n" +
                           "Verify input {SERVER}/{INSTANCE}";
                case 2:
                    return "An error has occurred while establishing a connection to the server\n" +
                           "Verify that the instance is correct and the SQL server is configured to allow remote connections.";
                case 67: // SQL Server timeout / problem connecting to the SQL server / could not open a connection to sql server
                    return "The server was not found or not accessible.\n" +
                           "Please verify that the instance name is correct";
                case 4060: // Invalid database
                    return "Invalid Database requested by login";
                case 18456: // Login failed
                    return "Login Failed for specified username";
                default:
                    return(exception.Message);
            }
        }

        public void ShowMessage(string message)
        {
            _writeMessage(message);
        }
    }
}
