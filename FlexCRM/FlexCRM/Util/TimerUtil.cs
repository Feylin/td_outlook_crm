﻿using System;
using System.Threading.Tasks;
using System.Timers;
using FlexCRM.BLL;

namespace FlexCRM.Util
{
    public class TimerUtil
    {
        private static readonly Lazy<TimerUtil> Lazy = new Lazy<TimerUtil>(() => new TimerUtil());

        public static TimerUtil Instance = Lazy.Value;

        private CalendarViewModel _cvm;
        private Timer _calendarTimer;
        private Timer _activityTimer;

        private TimerUtil()
        {
        }

        public void SetupTimers()
        {
            if (_cvm == null)
                _cvm = CalendarViewModel.Instance;

            if (_calendarTimer == null)
            {
                _calendarTimer = new Timer
                {
                    Interval = (int)TimeSpan.FromMinutes(5).TotalMilliseconds
                };
                _calendarTimer.Elapsed += CalendarTimerCallback;
            }
            if (_activityTimer == null)
            {
                _activityTimer = new Timer
                {
                    Interval = (int)TimeSpan.FromHours(1).TotalMilliseconds
                };
                _activityTimer.Elapsed += ActivityTimerCallback;
            }
        }

        private void CalendarTimerCallback(object sender, EventArgs e)
        {
            Task.Run(() => _cvm.SynchronizeCalendar());
        }

        private void ActivityTimerCallback(object sender, EventArgs e)
        {
            Task.Run(() =>_cvm.UpdateActivityDependencies());
        }

        public void DisposeTimers()
        {
            _calendarTimer?.Dispose();
            _activityTimer?.Dispose();
        }

        public void StartTimers()
        {
            if (_calendarTimer != null && _calendarTimer.Enabled == false)
                _calendarTimer.Start();
            if (_activityTimer != null && _activityTimer.Enabled == false)
                _activityTimer.Start();
        }
    }
}
